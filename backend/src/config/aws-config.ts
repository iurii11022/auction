export type AwsConfig = {
  region: string;
  accessKeyId: string;
  secretAccessKey: string;
  s3BucketName: string;
  s3BucketFolderName: string;
  maxImageSize: number;
  resizeImageWidth: number;
  resizeImageHeight: number;
};

export function getAWSConfig(): AwsConfig {
  return {
    region: process.env.AWS_REGION || '',
    accessKeyId: process.env.AWS_ACCESS_KEY_ID || '',
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY || '',
    s3BucketName: process.env.AWS_S3_BUCKET_NAME || 'sdsd',
    s3BucketFolderName: process.env.AWS_S3_BUCKET_FOLDER_NAME || '',
    maxImageSize: 650000000,
    resizeImageWidth: 400,
    resizeImageHeight: 300,
  };
}
