export type JwtConfig = {
  accessTokenSecret: string;
  accessTokenExp: number;
  refreshTokenSecret: string;
  refreshTokenExp: number;
};

export function getJwtConfig(): JwtConfig {
  return {
    accessTokenSecret: process.env.JWT_ACCESS_SECRET ?? 'secret',
    accessTokenExp: Number.isFinite(Number(process.env.JWT_ACCESS_EXP))
      ? Number(process.env.JWT_ACCESS_EXP)
      : 3000,
    refreshTokenSecret: process.env.JWT_REFRESH_SECRET ?? 'secret',
    refreshTokenExp: Number.isFinite(Number(process.env.JWT_REFRESH_EXP))
      ? Number(process.env.JWT_REFRESH_EXP)
      : 3000,
  };
}
