export type AppConfig = {
  readonly port: number;
  readonly url: string;
  readonly globalPrefix: string;
};

export function getAppConfig(): AppConfig {
  return {
    port: Number.isFinite(Number(process.env.BACKEND_PORT))
      ? Number(process.env.BACKEND_PORT)
      : 3000,
    url: process.env.BACKEND_URL || 'http://localhost',
    globalPrefix: process.env.GLOBAL_PREFIX || 'api',
  };
}
