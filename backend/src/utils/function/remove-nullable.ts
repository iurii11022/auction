export function removeNullable<T = Record<string, unknown>>(objToRemove: T): T {
  const obj = { ...objToRemove };
  for (const key in obj) {
    if (obj[key] === null || obj[key] === undefined) {
      delete obj[key];
    }
  }

  return obj;
}
