import { hashPassword } from '#src/utils/function/hash-password';
import { UnauthorizedException } from '@nestjs/common';

export async function verifyPassword(
  password: string,
  hash: string
): Promise<boolean> {
  const [salt, key] = hash.split(':');
  if (!salt || !key) {
    throw new UnauthorizedException('Invalid hash format');
  }

  const hashedPassword = await hashPassword(password, salt);
  const [_, hashedKey] = hashedPassword.split(':');

  return key === hashedKey;
}
