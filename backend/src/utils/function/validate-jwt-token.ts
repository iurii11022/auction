import { verify } from 'jsonwebtoken';
import { JwtToken } from '#src/modules/auth/models/jwtToken';
import { Company } from '#src/modules/company/models/company';
import { JwtConfig } from '#src/config/jwt-config';
import { UnauthorizedException } from '@nestjs/common';

export async function validateJwtToken(
  tokenString: string,
  jwtConfig: JwtConfig
): Promise<Company | undefined> {
  try {
    const [tokenType, token] = tokenString.split(' ');

    console.log(tokenType, 'tokenType');

    if (
      (tokenType !== JwtToken.ACCESS_TOKEN &&
        tokenType !== JwtToken.REFRESH_TOKEN) ||
      token === undefined
    ) {
      throw new UnauthorizedException(`invalid token`);
    }

    const decodedUserData = <Company>(
      await verify(
        token,
        tokenType === JwtToken.ACCESS_TOKEN
          ? jwtConfig.accessTokenSecret
          : jwtConfig.refreshTokenSecret
      )
    );

    if (!decodedUserData.id) {
      throw new UnauthorizedException(`invalid data format in token`);
    }

    return decodedUserData;
  } catch (e) {
    console.log(e, 'validateJwtToken ERROR');
    return undefined;
  }
}
