import { Injectable } from '@nestjs/common';

import { AuctionRepository } from '#src/modules/auction/dal/auction.repositry';
import { Auction } from '#src/modules/auction/models/auction/auction';
import { AuctionId } from '#src/modules/auction/models/auction/auction-id';
import { AuctionToCreate } from '#src/modules/auction/models/auction/auction-to-create';
import { AuctionToUpdate } from '#src/modules/auction/models/auction/auction-to-update';
import { Company } from '#src/modules/company/models/company';

@Injectable()
export class AuctionService {
  constructor(private questionRepo: AuctionRepository) {}

  create(createAuction: AuctionToCreate, company: Company): Promise<Auction> {
    return this.questionRepo.create(createAuction, company);
  }

  update(auctionToUpdate: AuctionToUpdate, company: Company): Promise<Auction> {
    return this.questionRepo.update(auctionToUpdate, company);
  }

  findMany(searchValue: string): Promise<Auction[]> {
    return this.questionRepo.findMany(searchValue);
  }

  getOneById(auctionId: AuctionId): Promise<Auction> {
    return this.questionRepo.getOneById(auctionId);
  }
}
