import { Auction } from '#src/modules/auction/models/auction/auction';
import { AuctionId } from '#src/modules/auction/models/auction/auction-id';
import { AuctionParticipantId } from '#src/modules/auction/models/auction-participant/auction-participant-id';
import { AuctionParticipantRole } from '#src/modules/auction/models/auction-participant/auction-participant-role';
import { Company } from '#src/modules/company/models/company';
import { CompanyId } from '#src/modules/company/models/company-id';

export type AuctionParticipant = {
  id: AuctionParticipantId;
  auction: Auction | AuctionId;
  company: Company | CompanyId;
  company_alias: string;
  role: AuctionParticipantRole;
  last_seen: Date;
};
