import { Opaque } from '#src/utils/opaque';

export type AuctionParticipantId = Opaque<string, 'AuctionParticipantId'>;
