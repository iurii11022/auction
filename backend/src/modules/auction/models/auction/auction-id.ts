import { Opaque } from '#src/utils/opaque';

export type AuctionId = Opaque<string, 'AuctionId'>;
