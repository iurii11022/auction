import { AuctionStatus } from '#src/modules/auction/models/auction/auction-status';
import { AuctionType } from '#src/modules/auction/models/auction/auction-type';
import { AuctionId } from '#src/modules/auction/models/auction/auction-id';

export type AuctionToUpdate = {
  id: AuctionId;
  name?: string;
  type?: AuctionType;
  description?: string;
  planned_start?: Date;
  planned_end?: Date;
  status?: AuctionStatus;
  object_name?: string;
  object_info?: string;
  desired_price?: number;
  min_price?: number;
  max_price?: number;
  additional_info?: string;
  main_photo?: string;
  object_photo_1?: string;
  object_photo_2?: string;
};
