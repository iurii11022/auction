import { AuctionType } from '#src/modules/auction/models/auction/auction-type';

export type AuctionToCreate = {
  name: string;
  type: AuctionType;
  description?: string;
  planned_start: Date;
  planned_end: Date;
};
