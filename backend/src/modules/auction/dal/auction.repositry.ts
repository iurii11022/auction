import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { AuctionEntity } from '#src/database/entities/auction.entity';
import { AuctionParticipantEntity } from '#src/database/entities/auction-participant.entity';
import { ChatEntity } from '#src/database/entities/chat.entity';
import { ChatMemberEntity } from '#src/database/entities/chat-member.entity';
import { CompanyEntity } from '#src/database/entities/company.entity';
import { AuctionDalMapper } from '#src/modules/auction/dal/auction.dal.mapper';
import { Auction } from '#src/modules/auction/models/auction/auction';
import { AuctionId } from '#src/modules/auction/models/auction/auction-id';
import { AuctionToCreate } from '#src/modules/auction/models/auction/auction-to-create';
import { AuctionToUpdate } from '#src/modules/auction/models/auction/auction-to-update';
import { AuctionParticipantRole } from '#src/modules/auction/models/auction-participant/auction-participant-role';
import { ChatType } from '#src/modules/chat/models/chat/chat-type';
import { Company } from '#src/modules/company/models/company';

@Injectable()
export class AuctionRepository {
  constructor(
    @InjectRepository(ChatEntity)
    private chat: Repository<ChatEntity>,
    @InjectRepository(AuctionEntity)
    private auction: Repository<AuctionEntity>,
    @InjectRepository(AuctionParticipantEntity)
    private auctionParticipant: Repository<AuctionParticipantEntity>,
    @InjectRepository(ChatMemberEntity)
    private chatMember: Repository<ChatMemberEntity>,
    private mapper: AuctionDalMapper
  ) {}
  async create(
    auctionToCreate: AuctionToCreate,
    company: Company
  ): Promise<Auction> {
    const createdAuctionEntity = await this.createAuction(
      auctionToCreate,
      company
    );
    const auctionParticipant = await this.createAuctionParticipant(
      createdAuctionEntity,
      company
    );
    const chat = await this.createChat(createdAuctionEntity);
    await this.createChatMember(chat, auctionParticipant);

    return await this.getOneById(createdAuctionEntity.id);
  }

  async findMany(searchValue: string): Promise<Auction[]> {
    const auctionEntities = await this.auction
      .createQueryBuilder('auction')
      .select()
      .leftJoin('auction.created_by', 'company')
      .addSelect('company.id')
      .addSelect('company.name')
      .leftJoinAndSelect('auction.chats', 'chat')
      .where('LOWER(auction.name) LIKE LOWER(:searchValue)', {
        searchValue: `%${searchValue.toLowerCase()}%`,
      })
      .getMany();

    return this.mapper.toAuctions(auctionEntities);
  }

  async getOneById(auctionId: AuctionId): Promise<Auction> {
    const auctionEntity = await this.auction
      .createQueryBuilder('auction')
      .select()
      .leftJoin('auction.created_by', 'company')
      .addSelect('company.id')
      .addSelect('company.name')
      .leftJoinAndSelect('auction.chats', 'chat')
      .where('auction.id = :auctionId', { auctionId })
      .getOne();

    if (!auctionEntity) {
      throw new NotFoundException('auction not found');
    }

    return this.mapper.auctionEntityToAuction(auctionEntity);
  }

  private async createAuction(
    auctionToCreate: AuctionToCreate,
    company: Company
  ): Promise<AuctionEntity> {
    const auctionData: Partial<AuctionEntity> = {
      name: auctionToCreate.name,
      type: auctionToCreate.type,
      description: auctionToCreate.description,
      planned_start: auctionToCreate.planned_start,
      planned_end: auctionToCreate.planned_end,
      created_by: { id: company.id } as unknown as CompanyEntity,
    };
    return await this.auction.save(auctionData);
  }

  public async update(
    auctionToUpdate: AuctionToUpdate,
    company: Company
  ): Promise<Auction> {
    const auctionData: Partial<AuctionEntity> = {
      name: auctionToUpdate.name,
      type: auctionToUpdate.type,
      description: auctionToUpdate.description,
      planned_start: auctionToUpdate.planned_start,
      planned_end: auctionToUpdate.planned_end,
      status: auctionToUpdate.status,
      object_name: auctionToUpdate.object_name,
      object_info: auctionToUpdate.object_info,
      desired_price: auctionToUpdate.desired_price,
      min_price: auctionToUpdate.min_price,
      max_price: auctionToUpdate.max_price,
      additional_info: auctionToUpdate.additional_info,
      main_photo: auctionToUpdate.main_photo,
      object_photo_1: auctionToUpdate.object_photo_1,
      object_photo_2: auctionToUpdate.object_photo_2,
      created_by: { id: company.id } as unknown as CompanyEntity,
    };

    await this.auction
      .createQueryBuilder()
      .update(AuctionEntity)
      .set(auctionData)
      .where('id = :auctionId', { auctionId: auctionToUpdate.id })
      .execute();

    return await this.getOneById(auctionToUpdate.id);
  }

  private async createAuctionParticipant(
    auctionEntity: AuctionEntity,
    company: Company
  ): Promise<AuctionParticipantEntity> {
    const auctionParticipantToCreate: Partial<AuctionParticipantEntity> = {
      auction: auctionEntity,
      company: { id: company.id } as unknown as CompanyEntity,
      company_alias: company.name,
      role: AuctionParticipantRole.BUYER,
      last_seen: new Date(),
    };
    return await this.auctionParticipant.save(auctionParticipantToCreate);
  }

  private async createChat(auctionEntity: AuctionEntity): Promise<ChatEntity> {
    const chatToCreate: Partial<ChatEntity> = {
      name: auctionEntity.name,
      type: ChatType.PUBLIC,
      auction: auctionEntity,
    };
    return await this.chat.save(chatToCreate);
  }

  private async createChatMember(
    chat: ChatEntity,
    auctionParticipant: AuctionParticipantEntity
  ): Promise<void> {
    const chatMemberToCreate: Partial<ChatMemberEntity> = {
      chat,
      auction_participant: auctionParticipant,
    };
    await this.chatMember.save(chatMemberToCreate);
  }
}
