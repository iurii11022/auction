import { Injectable } from '@nestjs/common';

import { AuctionEntity } from '#src/database/entities/auction.entity';
import { Auction } from '#src/modules/auction/models/auction/auction';
import { AuctionId } from '#src/modules/auction/models/auction/auction-id';
import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { Company } from '#src/modules/company/models/company';

@Injectable()
export class AuctionDalMapper {
  auctionEntityToAuction(entity: AuctionEntity): Auction {
    return {
      id: entity.id as AuctionId,
      name: entity.name,
      type: entity.type,
      planned_start: entity.planned_start,
      planned_end: entity.planned_end,
      object_name: entity.object_name,
      object_info: entity.object_info,
      desired_price: entity.desired_price,
      min_price: entity.min_price,
      max_price: entity.max_price,
      main_photo: entity.main_photo,
      object_photo_1: entity.object_photo_1,
      object_photo_2: entity.object_photo_2,
      description: entity.description,
      additional_info: entity.additional_info,
      current_price: entity.current_price,
      winner: entity.winner,
      status: entity.status,
      chats: entity.chats.map(c => ({
        id: c.id as ChatId,
        name: c.name,
        type: c.type,
        auction: c.auction_id as AuctionId,
      })),
      started_at: entity.started_at,
      completed_at: entity.completed_at,
      created_by: {
        id: entity.created_by.id,
        name: entity.created_by.name,
      } as Company,
    };
  }

  toAuctions(entities: AuctionEntity[]): Auction[] {
    return entities.map(auction => this.auctionEntityToAuction(auction));
  }
}
