import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuctionEntity } from '#src/database/entities/auction.entity';
import { AuctionParticipantEntity } from '#src/database/entities/auction-participant.entity';
import { ChatEntity } from '#src/database/entities/chat.entity';
import { ChatMemberEntity } from '#src/database/entities/chat-member.entity';
import { AuctionDalMapper } from '#src/modules/auction/dal/auction.dal.mapper';
import { AuctionRepository } from '#src/modules/auction/dal/auction.repositry';
import { AuctionController } from '#src/modules/auction/gateway/auction.controller';
import { AuctionMapper } from '#src/modules/auction/gateway/auction.mapper';
import { AuctionService } from '#src/modules/auction/services/auction.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      AuctionEntity,
      ChatEntity,
      AuctionParticipantEntity,
      ChatMemberEntity,
    ]),
  ],
  controllers: [AuctionController],
  providers: [
    AuctionMapper,
    AuctionDalMapper,
    AuctionService,
    AuctionRepository,
  ],
  exports: [AuctionService],
})
export class AuctionModule {}
