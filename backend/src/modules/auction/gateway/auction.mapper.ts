import { Injectable } from '@nestjs/common';

import { AuctionPresenation } from '#src/modules/auction/gateway/dto/auction.presentation';
import { AuctionToCreateDTO } from '#src/modules/auction/gateway/dto/auction-to-create.dto';
import { AuctionToUpdateDto } from '#src/modules/auction/gateway/dto/auction-to-update.dto';
import { Auction } from '#src/modules/auction/models/auction/auction';
import { AuctionToCreate } from '#src/modules/auction/models/auction/auction-to-create';
import { AuctionToUpdate } from '#src/modules/auction/models/auction/auction-to-update';
import { AuctionId } from '#src/modules/auction/models/auction/auction-id';

@Injectable()
export class AuctionMapper {
  auctionToCreateDTOToAuctionToCreate(
    createAuctionDto: AuctionToCreateDTO
  ): AuctionToCreate {
    return {
      name: createAuctionDto.name,
      type: createAuctionDto.type,
      description: createAuctionDto.description,
      planned_start: createAuctionDto.planned_start,
      planned_end: createAuctionDto.planned_end,
    };
  }

  auctionToUpdateDTOToAuctionToUpdate(
    createAuctionDto: AuctionToUpdateDto,
    auctionID: AuctionId
  ): AuctionToUpdate {
    return {
      id: auctionID,
      name: createAuctionDto.name,
      type: createAuctionDto.type,
      description: createAuctionDto.description,
      planned_start: createAuctionDto.planned_start,
      planned_end: createAuctionDto.planned_end,
      status: createAuctionDto.status,
      object_name: createAuctionDto.object_name,
      object_info: createAuctionDto.object_info,
      desired_price: createAuctionDto.desired_price,
      min_price: createAuctionDto.min_price,
      max_price: createAuctionDto.max_price,
      additional_info: createAuctionDto.additional_info,
      main_photo: createAuctionDto.main_photo,
      object_photo_1: createAuctionDto.object_photo_1,
      object_photo_2: createAuctionDto.object_photo_2,
    };
  }

  public toAuctionsPresentation(auctions: Auction[]): AuctionPresenation[] {
    return auctions.map(a => this.auctionToAuctionPresentation(a));
  }

  public auctionToAuctionPresentation(auction: Auction): AuctionPresenation {
    return {
      id: auction.id,
      name: auction.name,
      type: auction.type,
      description: auction.description,
      planned_start: auction.planned_start,
      planned_end: auction.planned_end,
      object_name: auction.object_name,
      object_info: auction.object_info,
      desired_price: auction.desired_price,
      min_price: auction.min_price,
      max_price: auction.max_price,
      current_price: auction.current_price,
      winner: auction.winner,
      additional_info: auction.additional_info,
      main_photo: auction.main_photo,
      object_photo_1: auction.object_photo_1,
      object_photo_2: auction.object_photo_2,
      status: auction.status,
      chats: auction.chats,
      started_at: auction.started_at,
      completed_at: auction.completed_at,
      created_by: auction.created_by,
    };
  }
}
