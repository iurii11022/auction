import { ApiProperty } from '@nestjs/swagger';

import { AuctionId } from '#src/modules/auction/models/auction/auction-id';
import { Chat } from '#src/modules/chat/models/chat/chat';
import { Company } from '#src/modules/company/models/company';
import { AuctionType } from '#src/modules/auction/models/auction/auction-type';
import { AuctionStatus } from '#src/modules/auction/models/auction/auction-status';

export class AuctionPresenation {
  @ApiProperty({
    description: 'auction',
    example: '38a40528-273e-498d-be75-9fe96a900cb2',
  })
  id!: AuctionId;

  @ApiProperty()
  name!: string;

  @ApiProperty()
  type!: AuctionType;

  @ApiProperty()
  description?: string;

  @ApiProperty()
  planned_start?: Date;

  @ApiProperty()
  planned_end?: Date;

  @ApiProperty()
  object_name?: string;

  @ApiProperty()
  object_info?: string;

  @ApiProperty()
  desired_price?: number;

  @ApiProperty()
  min_price?: number;

  @ApiProperty()
  current_price?: number;

  @ApiProperty()
  max_price?: number;

  @ApiProperty()
  additional_info?: string;

  @ApiProperty()
  main_photo?: string;

  @ApiProperty()
  object_photo_1?: string;

  @ApiProperty()
  object_photo_2?: string;

  @ApiProperty()
  status!: AuctionStatus;

  @ApiProperty()
  chats!: Chat[];

  @ApiProperty()
  started_at!: Date | null;

  @ApiProperty()
  completed_at!: Date | null;

  @ApiProperty()
  created_by!: Company;

  @ApiProperty()
  winner?: Company;
}
