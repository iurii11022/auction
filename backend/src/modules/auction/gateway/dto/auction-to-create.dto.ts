import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
  Validate,
} from 'class-validator';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'IsStartDateBeforeEndDate', async: false })
class IsStartDateBeforeEndDate implements ValidatorConstraintInterface {
  validate(startDate: any, args: ValidationArguments) {
    const obj = args.object as any;
    return startDate <= obj.planned_end;
  }

  defaultMessage(_args: ValidationArguments) {
    return 'planned_start must not be greater than planned_end';
  }
}

import { AuctionType } from '#src/modules/auction/models/auction/auction-type';

export class AuctionToCreateDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MinLength(5)
  @MaxLength(16)
  name!: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(AuctionType)
  type!: AuctionType;

  @ApiProperty()
  @IsOptional()
  @IsString()
  description?: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsDateString()
  @Validate(IsStartDateBeforeEndDate)
  planned_start!: Date;

  @ApiProperty()
  @IsNotEmpty()
  @IsDateString()
  planned_end!: Date;
}
