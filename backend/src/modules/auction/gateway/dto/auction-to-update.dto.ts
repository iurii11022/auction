import { ApiProperty } from '@nestjs/swagger';
import {
  IsNumber,
  IsOptional,
  IsString,
  Validate,
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

import { AuctionStatus } from '#src/modules/auction/models/auction/auction-status';
import { AuctionType } from '#src/modules/auction/models/auction/auction-type';

@ValidatorConstraint({ name: 'isLessThanDesired', async: false })
export class IsLessThanDesired implements ValidatorConstraintInterface {
  validate(minPrice: number, args: ValidationArguments) {
    const obj: any = args.object;
    return obj.desired_price >= minPrice;
  }

  defaultMessage(_args: ValidationArguments) {
    return 'Minimum price should be less than or equal to the desired price.';
  }
}

@ValidatorConstraint({ name: 'IsStartDateBeforeEndDate', async: false })
class IsMaxPriceBigger implements ValidatorConstraintInterface {
  validate(maxPrice: any, args: ValidationArguments) {
    const obj = args.object as any;
    return maxPrice > obj.min_price;
  }

  defaultMessage(_args: ValidationArguments) {
    return 'planned_start must not be greater than planned_end';
  }
}

export class AuctionToUpdateDto {
  @ApiProperty()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  type?: AuctionType;

  @ApiProperty()
  @IsOptional()
  @IsString()
  description?: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  planned_start?: Date;

  @ApiProperty()
  @IsOptional()
  @IsString()
  planned_end?: Date;

  @ApiProperty()
  @IsOptional()
  @IsString()
  status?: AuctionStatus;

  @ApiProperty()
  @IsOptional()
  @IsString()
  object_name?: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  object_info?: string;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  @Validate(Number)
  desired_price?: number;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  @Validate(Number)
  @Validate(IsLessThanDesired)
  min_price?: number;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  @Validate(Number)
  @Validate(IsMaxPriceBigger)
  max_price?: number;

  @ApiProperty()
  @IsOptional()
  @IsString()
  additional_info?: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  main_photo?: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  object_photo_1?: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  object_photo_2?: string;
}
