import {
  Body,
  Controller,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
  Put,
  Query,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { AuctionMapper } from '#src/modules/auction/gateway/auction.mapper';
import { AuctionPresenation } from '#src/modules/auction/gateway/dto/auction.presentation';
import { AuctionToCreateDTO } from '#src/modules/auction/gateway/dto/auction-to-create.dto';
import { AuctionToUpdateDto } from '#src/modules/auction/gateway/dto/auction-to-update.dto';
import { Auction } from '#src/modules/auction/models/auction/auction';
import { AuctionId } from '#src/modules/auction/models/auction/auction-id';
import { AuctionService } from '#src/modules/auction/services/auction.service';
import { AuthGuard } from '#src/modules/auth/guards/auth.guard';
import { CurrentCompany } from '#src/modules/company/decorators/company.decorator';
import { Company } from '#src/modules/company/models/company';

@UseGuards(AuthGuard)
@UsePipes(
  new ValidationPipe({
    transform: true,
    whitelist: true,
    forbidNonWhitelisted: true,
  })
)
@ApiTags('auction')
@Controller('auction')
export class AuctionController {
  constructor(
    private readonly auctionService: AuctionService,
    private readonly mapper: AuctionMapper
  ) {}

  @ApiResponse({
    status: 201,
    description: 'create new auction and return auction id',
    type: AuctionPresenation,
    isArray: false,
  })
  @Post()
  async createAuction(
    @CurrentCompany() company: Company,
    @Body() auction: AuctionToCreateDTO
  ): Promise<AuctionPresenation> {
    const createAuction =
      this.mapper.auctionToCreateDTOToAuctionToCreate(auction);
    const createdAuction: Auction = await this.auctionService.create(
      createAuction,
      company
    );
    return this.mapper.auctionToAuctionPresentation(createdAuction);
  }

  @ApiResponse({
    status: 200,
    description: 'update auction and return auction id',
    type: AuctionPresenation,
    isArray: false,
  })
  @Put(':auctionId')
  async updateAuction(
    @CurrentCompany() company: Company,
    @Body() auction: AuctionToUpdateDto,
    @Param('auctionId', new ParseUUIDPipe()) auctionID: AuctionId
  ): Promise<AuctionPresenation> {
    const auctionToUpdate = this.mapper.auctionToUpdateDTOToAuctionToUpdate(
      auction,
      auctionID
    );
    const createdAuction: Auction = await this.auctionService.update(
      auctionToUpdate,
      company
    );
    return this.mapper.auctionToAuctionPresentation(createdAuction);
  }

  @ApiResponse({
    status: 200,
    description: 'find auctions and return them',
    type: AuctionPresenation,
    isArray: true,
  })
  @Get()
  async findManyAuctions(
    @Query() query: { searchValue: string }
  ): Promise<AuctionPresenation[]> {
    const auctions: Auction[] = await this.auctionService.findMany(
      query.searchValue
    );

    return this.mapper.toAuctionsPresentation(auctions);
  }

  @ApiResponse({
    status: 200,
    description: 'find one auction by id and return it',
    type: AuctionPresenation,
    isArray: true,
  })
  @Get(':auctionId')
  async getOneAuctionById(
    @Param('auctionId', new ParseUUIDPipe()) auctionID: string
  ): Promise<AuctionPresenation> {
    const foundedAuction: Auction = await this.auctionService.getOneById(
      auctionID as AuctionId
    );
    return this.mapper.auctionToAuctionPresentation(foundedAuction);
  }
}
