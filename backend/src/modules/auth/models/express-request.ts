import { Request } from 'express';

import { Company } from '#src/modules/company/models/company';

export type ExpressRequest = Request & {
  company?: Company;
  companyForRefreshToken?: Company;
};
