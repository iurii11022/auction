import { Company } from '#src/modules/company/models/company';

export type UserWithTokens = Company & {
  accessToken: string;
  refreshToken: string;
};
