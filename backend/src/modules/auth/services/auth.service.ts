import { BadRequestException, Inject, Injectable } from '@nestjs/common';

import { getJwtConfig } from '#src/config/jwt-config';
import { AuthServiceInterface } from '#src/modules/auth/contracts/auth.service.interface';
import { LoginData } from '#src/modules/auth/models/login-data';
import { UserWithTokens } from '#src/modules/auth/models/user-with-tokens';
import { CompanyServiceInterface } from '#src/modules/company/contracts/company.service.interface';
import { COMPANY_SERVICE } from '#src/modules/company/contracts/company.servise.id';
import { Company } from '#src/modules/company/models/company';
import { RegisterData } from '#src/modules/company/models/register-data';
import { generateToken } from '#src/utils/function/generate-jwt-token';
import { verifyPassword } from '#src/utils/function/verify-password';

@Injectable()
export class AuthService implements AuthServiceInterface {
  private jwtConfig = getJwtConfig();
  constructor(
    @Inject(COMPANY_SERVICE) private userService: CompanyServiceInterface
  ) {}

  public async register(registerData: RegisterData): Promise<UserWithTokens> {
    const user = await this.userService.create(registerData);
    return this.getJwtTokens(user);
  }

  public async logIn(loginData: LoginData): Promise<UserWithTokens> {
    const { password, ...user } = await this.userService.findOneByEmail(
      loginData.email
    );
    const isPasswordValid = await verifyPassword(loginData.password, password);
    if (!isPasswordValid) throw new BadRequestException('wrong password');
    return this.getJwtTokens(user);
  }

  public async refreshTokens(company: Company): Promise<UserWithTokens> {
    return this.getJwtTokens(company);
  }

  private getJwtTokens(company: Company): UserWithTokens {
    return {
      ...company,
      accessToken:
        'access_token' +
        ' ' +
        generateToken(
          {
            id: company.id,
            email: company.email,
            name: company.name,
          },
          this.jwtConfig.accessTokenSecret,
          this.jwtConfig.accessTokenExp
        ),
      refreshToken:
        'refresh_token' +
        ' ' +
        generateToken(
          {
            id: company.id,
            email: company.email,
            name: company.name,
          },
          this.jwtConfig.refreshTokenSecret,
          this.jwtConfig.refreshTokenExp
        ),
    };
  }
}
