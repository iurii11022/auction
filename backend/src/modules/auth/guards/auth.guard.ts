import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';

import { ExpressRequest } from '#src/modules/auth/models/express-request';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest<ExpressRequest>();

    if (request.company) {
      return true;
    }

    throw new UnauthorizedException('This route cannot be reached');
  }
}
