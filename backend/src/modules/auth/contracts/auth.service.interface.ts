import { LoginData } from '#src/modules/auth/models/login-data';
import { UserWithTokens } from '#src/modules/auth/models/user-with-tokens';
import { Company } from '#src/modules/company/models/company';
import { RegisterData } from '#src/modules/company/models/register-data';

export interface AuthServiceInterface {
  register(user: RegisterData): Promise<UserWithTokens>;
  logIn(loginData: LoginData): Promise<UserWithTokens>;
  refreshTokens(user: Company): Promise<UserWithTokens>;
}
