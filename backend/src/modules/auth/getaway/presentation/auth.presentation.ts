import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class AuthPresentation {
  @ApiProperty({
    description: 'user',
    example: '38a40528-273e-498d-be75-9fe96a900cb2',
  })
  @IsNotEmpty()
  @IsString()
  public id!: string;

  @ApiProperty({ description: 'user email', example: 'user@gmail.com' })
  @IsNotEmpty()
  @IsString()
  public email!: string;

  @ApiProperty({ description: 'name of user', example: 'nic' })
  @IsNotEmpty()
  @IsString()
  public name!: string;

  @ApiProperty({ description: 'access token' })
  @IsNotEmpty()
  @IsString()
  public access_token!: string;

  @ApiProperty({ description: 'refresh token' })
  @IsNotEmpty()
  @IsString()
  public refresh_token!: string;
}
