import {
  Body,
  Controller,
  Inject,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { AuthPresentationMapper } from '#src/modules/auth/getaway/auth.presentation.mapper';
import { LoginDto } from '#src/modules/auth/getaway/dto/log-in.dto';
import { AuthPresentation } from '#src/modules/auth/getaway/presentation/auth.presentation';
import { AuthGuard } from '#src/modules/auth/guards/auth.guard';
import { UserWithTokens } from '#src/modules/auth/models/user-with-tokens';
import { CurrentCompany } from '#src/modules/company/decorators/company.decorator';
import { Company } from '#src/modules/company/models/company';

import { AuthServiceInterface } from '../contracts/auth.service.interface';
import { AUTH_SERVICE } from '../contracts/auth.servise.id';
import { AuthDtoMapper } from './auth.dto.mapper';
import { RegisterDto } from './dto/register.dto';

@UsePipes(
  new ValidationPipe({
    transform: true,
    whitelist: true,
    forbidNonWhitelisted: true,
  })
)
@ApiTags('auth')
@UsePipes(new ValidationPipe({ transform: true }))
@Controller('auth')
export class AuthController {
  constructor(
    @Inject(AUTH_SERVICE) private authService: AuthServiceInterface,
    private dtoMapper: AuthDtoMapper,
    private presMapper: AuthPresentationMapper
  ) {}

  @ApiResponse({
    status: 201,
    description: 'create new auth and return auth id',
    type: AuthPresentation,
    isArray: false,
  })
  @Post('register')
  async register(@Body() registerDto: RegisterDto): Promise<AuthPresentation> {
    const registerData = this.dtoMapper.registerDtoToRegisterData(registerDto);
    const user: UserWithTokens = await this.authService.register(registerData);
    return this.presMapper.userToAuthPresentation(user);
  }

  @ApiResponse({
    status: 200,
    description: 'find auths and return them',
    type: AuthPresentation,
    isArray: false,
  })
  @Post('login')
  async logIn(@Body() loginDto: LoginDto): Promise<AuthPresentation> {
    const createAuth = this.dtoMapper.logInDtoToLoginData(loginDto);
    const user: UserWithTokens = await this.authService.logIn(createAuth);
    return this.presMapper.userToAuthPresentation(user);
  }

  @UseGuards(AuthGuard)
  @ApiResponse({
    status: 200,
    description: 'refresh access and refresh tokens',
    type: AuthPresentation,
    isArray: false,
  })
  @Post('refresh-tokens')
  async refreshTokens(
    @CurrentCompany() company: Company
  ): Promise<AuthPresentation> {
    const userWithTokens = await this.authService.refreshTokens(company);
    return this.presMapper.userToAuthPresentation(userWithTokens);
  }
}
