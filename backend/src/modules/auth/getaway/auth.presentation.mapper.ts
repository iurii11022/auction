import { Injectable } from '@nestjs/common';

import { AuthPresentation } from '#src/modules/auth/getaway/presentation/auth.presentation';
import { UserWithTokens } from '#src/modules/auth/models/user-with-tokens';

@Injectable()
export class AuthPresentationMapper {
  userToAuthPresentation(user: UserWithTokens): AuthPresentation {
    return {
      id: user.id,
      email: user.email,
      name: user.name,
      access_token: user.accessToken,
      refresh_token: user.refreshToken,
    };
  }
}
