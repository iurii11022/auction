import { Injectable } from '@nestjs/common';

import { LoginDto } from '#src/modules/auth/getaway/dto/log-in.dto';
import { LoginData } from '#src/modules/auth/models/login-data';
import { RegisterData } from '#src/modules/company/models/register-data';

import { RegisterDto } from './dto/register.dto';

@Injectable()
export class AuthDtoMapper {
  registerDtoToRegisterData(registerDto: RegisterDto): RegisterData {
    return {
      email: registerDto.email,
      name: registerDto.name,
      password: registerDto.password,
    };
  }

  logInDtoToLoginData(loginDto: LoginDto): LoginData {
    return {
      email: loginDto.email,
      password: loginDto.password,
    };
  }
}
