import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class LoginDto {
  @ApiProperty({ description: 'auth email', example: 'auth@gmail.com' })
  @IsNotEmpty()
  @IsEmail()
  public email!: string;

  @ApiProperty({ description: 'auth password', example: 'qwerty' })
  @IsNotEmpty()
  @IsString()
  public password!: string;
}
