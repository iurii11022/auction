import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class RegisterDto {
  @ApiProperty({ description: 'user email', example: 'auth@gmail.com' })
  @IsNotEmpty()
  @IsEmail()
  public email!: string;

  @ApiProperty({ description: 'name of user', example: 'Nic' })
  @IsNotEmpty()
  @IsString()
  @MinLength(2)
  public name!: string;

  @ApiProperty({ description: 'user password', example: 'qwerty' })
  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @MaxLength(24)
  public password!: string;
}
