import { Module } from '@nestjs/common';

import { AUTH_SERVICE } from '#src/modules/auth/contracts/auth.servise.id';
import { AuthController } from '#src/modules/auth/getaway/auth.controller';
import { AuthDtoMapper } from '#src/modules/auth/getaway/auth.dto.mapper';
import { AuthPresentationMapper } from '#src/modules/auth/getaway/auth.presentation.mapper';
import { AuthService } from '#src/modules/auth/services/auth.service';
import { CompanyModule } from '#src/modules/company/company.module';

@Module({
  imports: [CompanyModule],
  controllers: [AuthController],
  providers: [
    AuthDtoMapper,
    AuthPresentationMapper,
    { useClass: AuthService, provide: AUTH_SERVICE },
  ],
  exports: [AUTH_SERVICE],
})
export class AuthModule {}
