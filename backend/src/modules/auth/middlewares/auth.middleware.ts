import {
  Inject,
  Injectable,
  NestMiddleware,
  UnauthorizedException,
} from '@nestjs/common';
import { NextFunction, Response } from 'express';

import { getJwtConfig } from '#src/config/jwt-config';
import { ExpressRequest } from '#src/modules/auth/models/express-request';
import { CompanyServiceInterface } from '#src/modules/company/contracts/company.service.interface';
import { COMPANY_SERVICE } from '#src/modules/company/contracts/company.servise.id';
import { Company } from '#src/modules/company/models/company';
import { validateJwtToken } from '#src/utils/function/validate-jwt-token';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  private jwtConfig = getJwtConfig();

  constructor(
    @Inject(COMPANY_SERVICE) private companyService: CompanyServiceInterface
  ) {}
  async use(req: ExpressRequest, _res: Response, next: NextFunction) {
    const tokenToValidate = req.headers['authorization'];
    if (!tokenToValidate) {
      req.company = undefined;
      return next();
    }

    const decodedUserData = await validateJwtToken(
      tokenToValidate,
      this.jwtConfig
    );

    if (!decodedUserData) {
      return next();
    }

    const company: Company = await this.companyService.findOneById(
      decodedUserData.id
    );

    if (!company) {
      throw new UnauthorizedException(`invalid data in token`);
    }

    req.company = company;

    next();
  }
}
