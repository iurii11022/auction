import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { S3 } from 'aws-sdk';
import { randomUUID } from 'crypto';
import * as sharp from 'sharp';
import slugify from 'slugify';

import { AwsConfig, getAWSConfig } from '#src/config/aws-config';

export type ImageType = {
  readonly fieldname: string;
  readonly originalname: string;
  readonly mimetype: string;
  buffer: Buffer;
  readonly size: number;
};

@Injectable()
export class AWSPictureHandler {
  awsConfig: AwsConfig;
  constructor() {
    this.awsConfig = getAWSConfig();
  }

  public async checkImage(image: ImageType): Promise<ImageType> {
    if (!image) {
      throw new HttpException(
        'image should not be empty',
        HttpStatus.BAD_REQUEST
      );
    }

    if (image.size > this.awsConfig.maxImageSize) {
      throw new HttpException(
        `image size must be less than ${this.awsConfig.maxImageSize / 1024}MB`,
        HttpStatus.UNPROCESSABLE_ENTITY
      );
    }

    if (
      image.mimetype !== 'image/jpeg' &&
      image.mimetype !== 'image/png' &&
      image.mimetype !== 'image/webp'
    ) {
      throw new HttpException(
        'image extension must be only .jpeg or .png',
        HttpStatus.UNPROCESSABLE_ENTITY
      );
    }

    return image;
  }

  public async uploadNewImageToAWSs3(
    file: ImageType,
    subFolderName: string
  ): Promise<any> {
    const { originalname, mimetype } = file;

    const bucket = this.getBucket();

    const linkToImage = this.pathBuilder(
      this.awsConfig.s3BucketFolderName,
      subFolderName,
      originalname
    );

    const params = {
      Bucket: this.awsConfig.s3BucketName,
      Key: linkToImage,
      Body: file.buffer,
      ContentType: mimetype,
    };

    return await bucket
      .upload(params)
      .promise()
      .catch(err => {
        throw new HttpException(
          `${err.message} can't upload image`,
          HttpStatus.SERVICE_UNAVAILABLE
        );
      })
      .then((data: S3.ManagedUpload.SendData) => data.Location);
  }

  async resizeImage(image: ImageType): Promise<ImageType> {
    image.buffer = await sharp(image.buffer)
      .resize(this.awsConfig.resizeImageWidth, this.awsConfig.resizeImageHeight)
      .sharpen()
      .toBuffer();
    return image;
  }

  private getBucket() {
    return new S3({
      region: this.awsConfig.region,
      accessKeyId: this.awsConfig.accessKeyId,
      secretAccessKey: this.awsConfig.secretAccessKey,
    });
  }

  private pathBuilder(
    rootFolderName: string,
    subFolderName: string,
    fileName: string
  ) {
    const uniqueFileName = this.getUniqueFileName(fileName);

    return `${rootFolderName}/${subFolderName}/${uniqueFileName}`;
  }

  private getUniqueFileName(originalName: string): string {
    const fileNameArr = originalName.split('.');
    const fileExtension = fileNameArr.pop();
    const fileName = fileNameArr[0];

    return (
      slugify(fileName + '-' + randomUUID(), {
        lower: true,
        remove: /[*+~.()'"!:@]/g,
      }) +
      '.' +
      fileExtension
    );
  }
}
