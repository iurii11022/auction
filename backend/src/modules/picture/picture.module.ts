import { Module } from '@nestjs/common';

import { AWSPictureHandler } from '#src/modules/picture/DAL/aws-picture.handler';
import { PICTURE_SERVICE } from '#src/modules/picture/DL/contracts/picture.servise.id';
import { PictureService } from '#src/modules/picture/DL/picture.service';
import { PictureController } from '#src/modules/picture/PL/picture.controller';
import { PicturePresentationMapper } from '#src/modules/picture/PL/picture.presentation.mapper';

@Module({
  controllers: [PictureController],
  providers: [
    PicturePresentationMapper,
    AWSPictureHandler,
    { useClass: PictureService, provide: PICTURE_SERVICE },
  ],
  exports: [PICTURE_SERVICE],
})
export class PictureModule {}
