import { Injectable } from '@nestjs/common';

import { PicturePresentation } from '#src/modules/picture/PL/presentation/picture.presentation';

@Injectable()
export class PicturePresentationMapper {
  public pictureUrlToPictureUrlPresentation(
    picture: string
  ): PicturePresentation {
    return {
      url: picture,
    };
  }
}
