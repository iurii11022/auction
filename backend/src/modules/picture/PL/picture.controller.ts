import {
  Controller,
  Delete,
  Inject,
  Param,
  ParseUUIDPipe,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { AuthGuard } from '#src/modules/auth/guards/auth.guard';
import { CurrentCompany } from '#src/modules/company/decorators/company.decorator';
import { Company } from '#src/modules/company/models/company';
import { PictureServiceInterface } from '#src/modules/picture/DL/contracts/picture.service.interface';
import { PICTURE_SERVICE } from '#src/modules/picture/DL/contracts/picture.servise.id';
import { PictureId } from '#src/modules/picture/DL/models/picture-id';
import { PicturePresentationMapper } from '#src/modules/picture/PL/picture.presentation.mapper';
import { PicturePresentation } from '#src/modules/picture/PL/presentation/picture.presentation';

@ApiTags('picture')
@UseGuards(AuthGuard)
@Controller('picture')
export class PictureController {
  constructor(
    @Inject(PICTURE_SERVICE) private pictureService: PictureServiceInterface,
    private presentationMapper: PicturePresentationMapper
  ) {}

  @ApiResponse({
    status: 201,
  })
  @Post()
  @UseInterceptors(FileInterceptor('image'))
  async createPicture(
    @CurrentCompany() company: Company,
    @UploadedFile() image: Array<Express.Multer.File>
  ): Promise<PicturePresentation> {
    const pictureUrl = await this.pictureService.create(image, company);

    return this.presentationMapper.pictureUrlToPictureUrlPresentation(
      pictureUrl
    );
  }

  @ApiResponse({
    status: 204,
  })
  @Delete(':pictureId')
  async deleteOnePictureById(
    @CurrentCompany() company: Company,
    @Param('pictureId', new ParseUUIDPipe()) pictureID: string
  ): Promise<any> {
    return this.pictureService.delete(pictureID as PictureId, company);
  }
}
