import { ApiProperty } from '@nestjs/swagger';

export class PicturePresentation {
  @ApiProperty()
  public url!: string;
}
