import { BadRequestException, Injectable } from '@nestjs/common';

import { Company } from '#src/modules/company/models/company';
import { AWSPictureHandler } from '#src/modules/picture/DAL/aws-picture.handler';
import { PictureServiceInterface } from '#src/modules/picture/DL/contracts/picture.service.interface';
import { PictureId } from '#src/modules/picture/DL/models/picture-id';

@Injectable()
export class PictureService implements PictureServiceInterface {
  constructor(private pictureHandler: AWSPictureHandler) {}

  async create(picture: any, company: Company): Promise<string> {
    const checkedImg = await this.pictureHandler.checkImage(picture);

    if (!checkedImg) {
      throw new BadRequestException();
    }

    const resizedImg = await this.pictureHandler.resizeImage(checkedImg);

    const subFolderPath = 'proj-plan-app' + '/' + company.id;

    return await this.pictureHandler.uploadNewImageToAWSs3(
      resizedImg,
      subFolderPath
    );
  }

  delete(_pictureID: PictureId, _company: Company): undefined {
    return undefined;
  }
}
