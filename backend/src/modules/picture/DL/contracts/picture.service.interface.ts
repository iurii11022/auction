import { Company } from '#src/modules/company/models/company';
import { PictureId } from '#src/modules/picture/DL/models/picture-id';

export interface PictureServiceInterface {
  create(picture: any, company: Company): Promise<string>;

  delete(pictureId: PictureId, company: Company): undefined;
}
