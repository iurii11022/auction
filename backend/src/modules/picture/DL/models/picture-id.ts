import { Opaque } from '#src/utils/opaque';

export type PictureId = Opaque<string, 'PictureId'>;
