import {
  Inject,
  NotFoundException,
  OnModuleInit,
  UnauthorizedException,
} from '@nestjs/common';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

import { WebSocketEvent } from '#src/modules/chat/gateway/models/client-event';
import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { ChatMessageToCreate } from '#src/modules/chat/models/chat-message/chat-message-to-create';
import { CurrentCompanyWS } from '#src/modules/company/decorators/company-ws.decorator';
import { Company } from '#src/modules/company/models/company';
import { validateJwtToken } from '#src/utils/function/validate-jwt-token';
import { getJwtConfig } from '#src/config/jwt-config';
import { COMPANY_SERVICE } from '#src/modules/company/contracts/company.servise.id';
import { CompanyServiceInterface } from '#src/modules/company/contracts/company.service.interface';
import { EventRouter } from '#src/modules/chat/event.router';
import { JoinToChat } from '#src/modules/chat/models/chat-message/join-to-chat';
import { ChatMessageStop } from '#src/modules/chat/models/chat-message/chat-message-stop';
import { ChatMessageStart } from '#src/modules/chat/models/chat-message/chat-message-start';

@WebSocketGateway({ cors: true })
export class WebsocketEventGateway
  implements OnGatewayConnection, OnGatewayDisconnect, OnModuleInit
{
  private jwtConfig = getJwtConfig();
  @WebSocketServer() server!: Server;

  constructor(
    @Inject(COMPANY_SERVICE) private companyService: CompanyServiceInterface,
    private readonly eventRouter: EventRouter
  ) {}

  async onModuleInit() {}

  async handleConnection(socket: Socket) {
    try {
      const tokenToValidate = socket.handshake.headers['authorization'];
      if (!tokenToValidate) {
        throw new UnauthorizedException('no token ih headers authorization');
      }

      const validatedCompanyData = await validateJwtToken(
        tokenToValidate,
        this.jwtConfig
      );

      if (!validatedCompanyData) {
        throw new UnauthorizedException('invalid validated Company Data');
      }

      const company: Company = await this.companyService.findOneById(
        validatedCompanyData.id
      );

      if (!company) {
        throw new NotFoundException(`Cant find company`);
      }

      socket.data.company = company;
    } catch (e) {
      console.error(e, 'handleConnection Error');
    }
  }

  async handleDisconnect(socket: Socket) {
    socket.disconnect();
  }

  @SubscribeMessage(WebSocketEvent.SEND_MESSAGE)
  async message(
    @MessageBody() clientMessage: ChatMessageToCreate,
    @CurrentCompanyWS() company: Company
  ): Promise<void> {
    const serverMessage = await this.eventRouter.handle(
      WebSocketEvent.SEND_MESSAGE,
      clientMessage,
      company
    );

    this.server
      .to(clientMessage.chat_id)
      .emit(WebSocketEvent.MESSAGE, serverMessage);
  }

  @SubscribeMessage(WebSocketEvent.SEND_BET_MESSAGE)
  async betMessage(
    @MessageBody() clientMessage: ChatMessageToCreate,
    @CurrentCompanyWS() company: Company
  ): Promise<void> {
    const serverMessage = await this.eventRouter.handle(
      WebSocketEvent.SEND_BET_MESSAGE,
      clientMessage,
      company
    );

    this.server
      .to(clientMessage.chat_id)
      .emit(WebSocketEvent.MESSAGE, serverMessage);
  }

  @SubscribeMessage(WebSocketEvent.JOIN_TO_CHAT)
  async joinToChat(
    @MessageBody() clientMessage: JoinToChat,
    @CurrentCompanyWS() company: Company
  ): Promise<void> {
    const serverMessage = await this.eventRouter.handle(
      WebSocketEvent.JOIN_TO_CHAT,
      clientMessage,
      company
    );

    this.server
      .to(clientMessage.chat_id)
      .emit(WebSocketEvent.MESSAGE, serverMessage);
  }

  @SubscribeMessage(WebSocketEvent.START_AUCTION)
  async startAuction(
    @MessageBody() clientMessage: ChatMessageStart,
    @CurrentCompanyWS() company: Company
  ): Promise<void> {
    const serverMessage = await this.eventRouter.handle(
      WebSocketEvent.START_AUCTION,
      clientMessage,
      company
    );

    this.server
      .to(clientMessage.chat_id)
      .emit(WebSocketEvent.MESSAGE, serverMessage);
  }

  @SubscribeMessage(WebSocketEvent.STOP_AUCTION)
  async stopAuction(
    @MessageBody() clientMessage: ChatMessageStop,
    @CurrentCompanyWS() company: Company
  ): Promise<void> {
    const serverMessage = await this.eventRouter.handle(
      WebSocketEvent.STOP_AUCTION,
      clientMessage,
      company
    );

    this.server
      .to(clientMessage.chat_id)
      .emit(WebSocketEvent.MESSAGE, serverMessage);
  }

  @SubscribeMessage(WebSocketEvent.GET_CHAT)
  async chat(
    @ConnectedSocket() socket: Socket,
    @MessageBody() chatid: ChatId,
    @CurrentCompanyWS() company: Company
  ): Promise<void> {
    socket.join(chatid);
    const serverMessage = await this.eventRouter.handle(
      WebSocketEvent.GET_CHAT,
      chatid,
      company
    );

    socket.emit(WebSocketEvent.CHAT_WITH_DATA, serverMessage);
  }
}
