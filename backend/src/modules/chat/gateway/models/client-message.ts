import { WSClientMessage } from '#src/modules/chat/gateway/models/ws-client-message';

export type ClientMessage = WSClientMessage;
