import { Chat } from '#src/modules/chat/models/chat/chat';
import { ChatWithData } from '#src/modules/chat/models/chat/chat-with-data';
import { ChatMessageWithAuthor } from '#src/modules/chat/models/chat-message/chat-message-with-author';
import { ChatMessageNewMember } from '#src/modules/chat/models/chat-message/chat-message-new-member';

export type ServerMessage =
  | Chat
  | ChatWithData
  | ChatMessageWithAuthor
  | ChatMessageNewMember;
