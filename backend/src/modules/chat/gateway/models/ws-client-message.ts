import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { ChatToCreate } from '#src/modules/chat/models/chat/chat-to-create';
import { ChatMessageToCreate } from '#src/modules/chat/models/chat-message/chat-message-to-create';
import { JoinToChat } from '#src/modules/chat/models/chat-message/join-to-chat';

export type WSClientMessage =
  | ChatToCreate
  | ChatMessageToCreate
  | ChatId
  | JoinToChat;
