import { Socket } from 'socket.io';

export type Client = Socket;
