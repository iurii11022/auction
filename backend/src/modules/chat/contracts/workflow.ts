import { ClientMessage } from '#src/modules/chat/gateway/models/client-message';
import { ServerMessage } from '#src/modules/chat/gateway/models/server-message';
import { Company } from '#src/modules/company/models/company';

export interface Workflow {
  run(message: ClientMessage, company: Company): Promise<ServerMessage>;
}
