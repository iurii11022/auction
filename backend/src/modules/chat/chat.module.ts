import { Module } from '@nestjs/common';

import { WebsocketEventGateway } from '#src/modules/chat/gateway/websocket-event.gateway';
import { CompanyModule } from '#src/modules/company/company.module';
import { EventRouter } from '#src/modules/chat/event.router';
import { MessageWorkflow } from '#src/modules/chat/workflows/message.workflow';
import { OpenChatWorkflow } from '#src/modules/chat/workflows/open-chat.workflow';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatMessageEntity } from '#src/database/entities/chat-message.entity';
import { ChatEntity } from '#src/database/entities/chat.entity';
import { AuctionEntity } from '#src/database/entities/auction.entity';
import { AuctionParticipantEntity } from '#src/database/entities/auction-participant.entity';
import { ChatMemberEntity } from '#src/database/entities/chat-member.entity';
import { JoinToChatWorkflow } from '#src/modules/chat/workflows/join-to-chat.workflow';
import { StartMessageWorkflow } from '#src/modules/chat/workflows/start-message.workflow';
import { StopMessageWorkflow } from '#src/modules/chat/workflows/stop-message.workflow';
import { BetMessageWorkflow } from '#src/modules/chat/workflows/bet-message.workflow';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ChatEntity,
      ChatMessageEntity,
      AuctionEntity,
      AuctionParticipantEntity,
      ChatEntity,
      ChatMessageEntity,
      ChatMemberEntity,
    ]),
    CompanyModule,
  ],
  providers: [
    WebsocketEventGateway,
    EventRouter,
    MessageWorkflow,
    OpenChatWorkflow,
    JoinToChatWorkflow,
    StartMessageWorkflow,
    StopMessageWorkflow,
    BetMessageWorkflow,
  ],
  exports: [],
})
export class ChatModule {}
