import { BadRequestException, Injectable } from '@nestjs/common';

import { Workflow } from '#src/modules/chat/contracts/workflow';
import {
  ClientEvent,
  WebSocketEvent,
} from '#src/modules/chat/gateway/models/client-event';
import { ServerMessage } from '#src/modules/chat/gateway/models/server-message';
import { WSClientMessage } from '#src/modules/chat/gateway/models/ws-client-message';
import { MessageWorkflow } from '#src/modules/chat/workflows/message.workflow';
import { OpenChatWorkflow } from '#src/modules/chat/workflows/open-chat.workflow';
import { Company } from '#src/modules/company/models/company';
import { JoinToChatWorkflow } from '#src/modules/chat/workflows/join-to-chat.workflow';
import { StartMessageWorkflow } from '#src/modules/chat/workflows/start-message.workflow';
import { StopMessageWorkflow } from '#src/modules/chat/workflows/stop-message.workflow';
import { BetMessageWorkflow } from '#src/modules/chat/workflows/bet-message.workflow';

@Injectable()
export class EventRouter {
  private readonly workflows: Map<ClientEvent, Workflow>;

  constructor(
    private readonly openChatWorkflow: OpenChatWorkflow,
    private readonly messageWorkflow: MessageWorkflow,
    private readonly joinToChatWorkflow: JoinToChatWorkflow,
    private readonly startMessageWorkflow: StartMessageWorkflow,
    private readonly stopMessageWorkflow: StopMessageWorkflow,
    private readonly betMessageWorkflow: BetMessageWorkflow
  ) {
    this.workflows = new Map<ClientEvent, Workflow>([
      [WebSocketEvent.GET_CHAT, this.openChatWorkflow],
      [WebSocketEvent.SEND_MESSAGE, this.messageWorkflow],
      [WebSocketEvent.JOIN_TO_CHAT, this.joinToChatWorkflow],
      [WebSocketEvent.START_AUCTION, this.startMessageWorkflow],
      [WebSocketEvent.STOP_AUCTION, this.stopMessageWorkflow],
      [WebSocketEvent.SEND_BET_MESSAGE, this.betMessageWorkflow],
    ]);
  }

  handle(
    event: ClientEvent,
    message: WSClientMessage,
    company: Company
  ): Promise<ServerMessage> {
    const workflow = this.determineWorkflow(event);

    return workflow.run(message, company);
  }

  determineWorkflow(event: ClientEvent): Workflow {
    const workflow = this.workflows.get(event);

    if (!workflow) {
      throw new BadRequestException('workflow is not determined');
    }

    return workflow;
  }
}
