import { Chat } from '#src/modules/chat/models/chat/chat';
import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { ChatMember } from '#src/modules/chat/models/chat-member/chat-member';
import { ChatMemberId } from '#src/modules/chat/models/chat-member/chat-member-id';
import { ChatMessageId } from '#src/modules/chat/models/chat-message/chat-message-id';

export type ChatMessage = {
  id: ChatMessageId;
  chat: Chat | ChatId;
  created_at: Date;
  author: ChatMember | ChatMemberId;
  text_content: string;
};
