import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { ChatMemberId } from '#src/modules/chat/models/chat-member/chat-member-id';
import {AuctionId} from "#src/modules/auction/models/auction/auction-id";

export type ChatMessageStart = {
  chat_id: ChatId;
  author_id: ChatMemberId;
  auction_id: AuctionId;
};
