export enum ChatMessageType {
  MESSAGE = 'message',
  BET_MESSAGE = 'bet message',
  START_AUCTION = 'start auction',
  STOP_AUCTION = 'stop auction',
  NEW_MEMBER = 'new member',
}
