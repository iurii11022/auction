import { Opaque } from '#src/utils/opaque';

export type ChatMessageId = Opaque<string, 'ChatMessageId'>;
