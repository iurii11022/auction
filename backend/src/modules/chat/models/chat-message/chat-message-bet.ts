import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { ChatMemberId } from '#src/modules/chat/models/chat-member/chat-member-id';
import { AuctionId } from '#src/modules/auction/models/auction/auction-id';

export type ChatMessageBet = {
  chat_id: ChatId;
  author_id: ChatMemberId;
  text_content: string;
  auction_id: AuctionId;
};
