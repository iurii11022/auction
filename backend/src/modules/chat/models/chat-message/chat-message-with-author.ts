import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { ChatMessageId } from '#src/modules/chat/models/chat-message/chat-message-id';
import { ChatMessageType } from '#src/modules/chat/models/chat-message/chat-message-type';

export type ChatMessageWithAuthor = {
  id: ChatMessageId;
  chat_id: ChatId;
  message_author: string;
  message_author_id: string;
  text_content: string;
  created_at: Date;
  type:
    | ChatMessageType.MESSAGE
    | ChatMessageType.START_AUCTION
    | ChatMessageType.STOP_AUCTION
    | ChatMessageType.BET_MESSAGE;
};
