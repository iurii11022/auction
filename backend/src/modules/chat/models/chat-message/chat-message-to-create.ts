import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { ChatMemberId } from '#src/modules/chat/models/chat-member/chat-member-id';

export type ChatMessageToCreate = {
  chat_id: ChatId;
  author_id: ChatMemberId;
  text_content: string;
};
