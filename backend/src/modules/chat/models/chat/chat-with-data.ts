import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { ChatType } from '#src/modules/chat/models/chat/chat-type';
import { ChatMember } from '#src/modules/chat/models/chat-member/chat-member';
import { ChatMessageWithAuthor } from '#src/modules/chat/models/chat-message/chat-message-with-author';
import { Auction } from '#src/modules/auction/models/auction/auction';

export type ChatWithData = {
  id: ChatId;
  name: string;
  type: ChatType;
  auction: Auction;
  chat_members: ChatMember[];
  chat_messages: ChatMessageWithAuthor;
  current_chat_member?: ChatMember;
  is_auction_owner?: boolean;
};
