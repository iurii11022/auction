import { Auction } from '#src/modules/auction/models/auction/auction';
import { AuctionId } from '#src/modules/auction/models/auction/auction-id';
import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { ChatType } from '#src/modules/chat/models/chat/chat-type';

export type Chat = {
  id: ChatId;
  name: string;
  type: ChatType;
  auction: Auction | AuctionId;
};
