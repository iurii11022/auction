import { AuctionId } from '#src/modules/auction/models/auction/auction-id';
import { ChatType } from '#src/modules/chat/models/chat/chat-type';

export type ChatToCreate = {
  name: string;
  type: ChatType;
  auction_id: AuctionId;
};
