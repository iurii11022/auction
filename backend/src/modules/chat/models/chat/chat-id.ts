import { Opaque } from '#src/utils/opaque';

export type ChatId = Opaque<string, 'ChatId'>;
