import { AuctionParticipantId } from '#src/modules/auction/models/auction-participant/auction-participant-id';
import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { ChatMemberId } from '#src/modules/chat/models/chat-member/chat-member-id';

export type ChatMemberToCreate = {
  id: ChatMemberId;
  chat_id: ChatId;
  auction_participant_d: AuctionParticipantId;
};
