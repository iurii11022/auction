import { AuctionParticipant } from '#src/modules/auction/models/auction-participant/auction-participant';
import { AuctionParticipantId } from '#src/modules/auction/models/auction-participant/auction-participant-id';
import { Chat } from '#src/modules/chat/models/chat/chat';
import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { ChatMemberId } from '#src/modules/chat/models/chat-member/chat-member-id';

export type ChatMember = {
  id: ChatMemberId;
  chat: Chat | ChatId;
  auction_participant: AuctionParticipant | AuctionParticipantId;
};
