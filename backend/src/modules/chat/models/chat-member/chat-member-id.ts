import { Opaque } from '#src/utils/opaque';

export type ChatMemberId = Opaque<string, 'ChatMemberId'>;
