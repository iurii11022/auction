import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Equal, Repository } from 'typeorm';

import { ChatEntity } from '#src/database/entities/chat.entity';
import { ChatMemberEntity } from '#src/database/entities/chat-member.entity';
import { ChatMessageEntity } from '#src/database/entities/chat-message.entity';
import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { Company } from '#src/modules/company/models/company';
import { ChatMessageType } from '#src/modules/chat/models/chat-message/chat-message-type';
import { JoinToChat } from '#src/modules/chat/models/chat-message/join-to-chat';
import { AuctionEntity } from '#src/database/entities/auction.entity';
import { AuctionParticipantEntity } from '#src/database/entities/auction-participant.entity';
import { CompanyEntity } from '#src/database/entities/company.entity';
import { AuctionParticipantRole } from '#src/modules/auction/models/auction-participant/auction-participant-role';
import { ChatMessageNewMember } from '#src/modules/chat/models/chat-message/chat-message-new-member';
import { Workflow } from '#src/modules/chat/contracts/workflow';
import { ChatMemberId } from '#src/modules/chat/models/chat-member/chat-member-id';

@Injectable()
export class JoinToChatWorkflow implements Workflow {
  constructor(
    @InjectRepository(AuctionEntity)
    private auction: Repository<AuctionEntity>,
    @InjectRepository(AuctionParticipantEntity)
    private auctionParticipant: Repository<AuctionParticipantEntity>,
    @InjectRepository(ChatEntity)
    private chat: Repository<ChatEntity>,
    @InjectRepository(ChatMessageEntity)
    private message: Repository<ChatMessageEntity>,
    @InjectRepository(ChatMemberEntity)
    private chatMember: Repository<ChatMemberEntity>
  ) {}

  async run(
    message: JoinToChat,
    company: Company
  ): Promise<ChatMessageNewMember> {
    const chat = await this.chat.findOne({
      where: { id: message.chat_id },
    });

    if (!chat) {
      throw new BadRequestException();
    }

    const foundedAuctionEntity = await this.auction.findOne({
      // @ts-ignore
      where: { id: Equal(chat.auction_id) },
    });

    if (!foundedAuctionEntity) {
      throw new BadRequestException();
    }

    const auctionParticipant = await this.createAuctionParticipant(
      foundedAuctionEntity,
      company
    );

    const chatMember = await this.createChatMember(chat, auctionParticipant);

    const messageToCreate: Partial<ChatMessageEntity> = {
      text_content: 'new auction member joined to chat!!!',
      type: ChatMessageType.NEW_MEMBER,
      chat,
      created_at: new Date(),
      author: chatMember,
    };

    const createdMessage = await this.message.save(messageToCreate);

    const retrievedMessage = await this.message
      .createQueryBuilder('message')
      .leftJoinAndSelect('message.author', 'author')
      .leftJoinAndSelect('author.auction_participant', 'auctionParticipant')
      .where('message.id = :id', { id: createdMessage.id })
      .getOne();

    if (!retrievedMessage) {
      throw new NotFoundException('Message not found');
    }

    return {
      type: ChatMessageType.NEW_MEMBER,
      id: company.id,
      text_content: retrievedMessage.text_content,
      chat_id: retrievedMessage.chat_id as ChatId,
      message_author: auctionParticipant.company_alias,
      name: auctionParticipant.company_alias,
      created_at: retrievedMessage.created_at,
      chat_member_id: chatMember.id as ChatMemberId,
    };
  }

  private async createAuctionParticipant(
    auctionEntity: AuctionEntity,
    company: Company
  ): Promise<AuctionParticipantEntity> {
    const auctionParticipantToCreate: Partial<AuctionParticipantEntity> = {
      auction: auctionEntity,
      company: { id: company.id } as unknown as CompanyEntity,
      company_alias: company.name,
      role: AuctionParticipantRole.BUYER,
      last_seen: new Date(),
    };
    return await this.auctionParticipant.save(auctionParticipantToCreate);
  }

  private async createChatMember(
    chat: ChatEntity,
    auctionParticipant: AuctionParticipantEntity
  ): Promise<ChatMemberEntity> {
    const chatMemberToCreate: Partial<ChatMemberEntity> = {
      chat,
      auction_participant: auctionParticipant,
    };
    return await this.chatMember.save(chatMemberToCreate);
  }
}
