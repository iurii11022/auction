import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { ChatEntity } from '#src/database/entities/chat.entity';
import { ChatMemberEntity } from '#src/database/entities/chat-member.entity';
import { ChatMessageEntity } from '#src/database/entities/chat-message.entity';
import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { ChatMessageId } from '#src/modules/chat/models/chat-message/chat-message-id';
import { ChatMessageToCreate } from '#src/modules/chat/models/chat-message/chat-message-to-create';
import { ChatMessageWithAuthor } from '#src/modules/chat/models/chat-message/chat-message-with-author';
import { Company } from '#src/modules/company/models/company';
import { ChatMessageType } from '#src/modules/chat/models/chat-message/chat-message-type';

@Injectable()
export class MessageWorkflow {
  constructor(
    @InjectRepository(ChatMessageEntity)
    private message: Repository<ChatMessageEntity>
  ) {}

  async run(
    message: ChatMessageToCreate,
    _company: Company
  ): Promise<ChatMessageWithAuthor> {
    const messageToCreate: Partial<ChatMessageEntity> = {
      type: ChatMessageType.MESSAGE,
      chat: { id: message.chat_id } as unknown as ChatEntity,
      created_at: new Date(),
      author: { id: message.author_id } as unknown as ChatMemberEntity,
      text_content: message.text_content,
    };

    const createdMessage = await this.message.save(messageToCreate);

    const retrievedMessage = await this.message
      .createQueryBuilder('message')
      .leftJoinAndSelect('message.author', 'author')
      .leftJoinAndSelect('author.auction_participant', 'auctionParticipant')
      .where('message.id = :id', { id: createdMessage.id })
      .getOne();

    if (!retrievedMessage) {
      throw new NotFoundException('Message not found');
    }

    return {
      type: retrievedMessage.type as ChatMessageType.MESSAGE,
      id: retrievedMessage.id as ChatMessageId,
      chat_id: retrievedMessage.chat_id as ChatId,
      message_author: retrievedMessage.author.auction_participant.company_alias,
      message_author_id: retrievedMessage.author_id,
      text_content: retrievedMessage.text_content,
      created_at: retrievedMessage.created_at,
    };
  }
}
