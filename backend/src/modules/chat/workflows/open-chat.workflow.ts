import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { ChatEntity } from '#src/database/entities/chat.entity';
import { ChatMemberEntity } from '#src/database/entities/chat-member.entity';
import { ChatMessageEntity } from '#src/database/entities/chat-message.entity';
import { Workflow } from '#src/modules/chat/contracts/workflow';
import { AuctionParticipant } from '#src/modules/auction/models/auction-participant/auction-participant';
import { AuctionParticipantId } from '#src/modules/auction/models/auction-participant/auction-participant-id';
import { ChatId } from '#src/modules/chat/models/chat/chat-id';
import { ChatWithData } from '#src/modules/chat/models/chat/chat-with-data';
import { ChatMember } from '#src/modules/chat/models/chat-member/chat-member';
import { ChatMemberId } from '#src/modules/chat/models/chat-member/chat-member-id';
import { ChatMessageId } from '#src/modules/chat/models/chat-message/chat-message-id';
import { ChatMessageWithAuthor } from '#src/modules/chat/models/chat-message/chat-message-with-author';
import { Auction } from '#src/modules/auction/models/auction/auction';
import { Company } from '#src/modules/company/models/company';
import { CompanyId } from '#src/modules/company/models/company-id';

@Injectable()
export class OpenChatWorkflow implements Workflow {
  constructor(
    @InjectRepository(ChatEntity) private chat: Repository<ChatEntity>
  ) {}

  async run(chatId: ChatId, company: Company): Promise<ChatWithData> {
    const chat = await this.chat
      .createQueryBuilder('chat')
      .leftJoinAndSelect('chat.chat_members', 'members')
      .leftJoinAndSelect('chat.auction', 'auction')
      .leftJoinAndSelect('members.auction_participant', 'auctionParticipant')
      .leftJoinAndSelect('chat.chat_messages', 'messages')
      .leftJoin('messages.author', 'author')
      .addSelect('author.id')
      .leftJoin('author.auction_participant', 'ap')
      .addSelect('ap.company_alias')
      .where('chat.id = :chatId', { chatId })
      .getOne();

    if (!chat) {
      throw new NotFoundException('Chat not found');
    }

    // const util = require('util');
    // console.log(util.inspect(chat, { showHidden: false, depth: null }));

    return {
      id: chat.id as ChatId,
      name: chat.name,
      type: chat.type,
      auction: {
        id: chat.auction.id,
        name: chat.auction.name,
        type: chat.auction.type,
        description: chat.auction.description,
        planned_start: chat.auction.planned_start,
        planned_end: chat.auction.planned_end,
        object_name: chat.auction.object_name,
        object_info: chat.auction.object_info,
        desired_price: chat.auction.desired_price,
        min_price: chat.auction.min_price,
        max_price: chat.auction.max_price,
        additional_info: chat.auction.additional_info,
        main_photo: chat.auction.main_photo,
        object_photo_1: chat.auction.object_photo_1,
        object_photo_2: chat.auction.object_photo_2,
        status: chat.auction.status,
        started_at: chat.auction.started_at,
        completed_at: chat.auction.completed_at,
        created_by: chat.auction.created_by,
      } as unknown as Auction,
      chat_members: chat.chat_members.map(
        (chatMemberEntity: ChatMemberEntity) => ({
          id: chatMemberEntity.id as ChatMemberId,
          chat: chatMemberEntity.chat_id as ChatId,
          auction_participant: {
            id: chatMemberEntity.auction_participant_id,
            company_alias: chatMemberEntity.auction_participant.company_alias,
            role: chatMemberEntity.auction_participant.role,
            last_seen: chatMemberEntity.auction_participant.last_seen,
          } as unknown as AuctionParticipant,
        })
      ),
      chat_messages: chat.chat_messages.map(
        (chatMessageEntity: ChatMessageEntity) => ({
          id: chatMessageEntity.id as ChatMessageId,
          type: chatMessageEntity.type,
          chat_id: chatMessageEntity.chat_id as ChatId,
          message_author_id: chatMessageEntity.author_id,
          message_author:
            chatMessageEntity.author.auction_participant.company_alias,
          text_content: chatMessageEntity.text_content,
          created_at: chatMessageEntity.created_at,
        })
      ) as unknown as ChatMessageWithAuthor,
      current_chat_member: this.findCurrentChatMember(chat, company.id),
      is_auction_owner: chat.auction.created_by_id === company.id,
    };
  }

  private findCurrentChatMember(
    chat: ChatEntity,
    companyId: CompanyId
  ): ChatMember | undefined {
    const chatMemberEntity = chat.chat_members.find(
      member => member.auction_participant.company_id === companyId
    );

    if (!chatMemberEntity) {
      return undefined;
    }

    return {
      id: chatMemberEntity.id as ChatMemberId,
      chat: chatMemberEntity.chat_id as ChatId,
      auction_participant: {
        id: chatMemberEntity.auction_participant_id as AuctionParticipantId,
        company_alias: chatMemberEntity.auction_participant.company_alias,
        role: chatMemberEntity.auction_participant.role,
        last_seen: chatMemberEntity.auction_participant.last_seen,
      } as AuctionParticipant,
    };
  }
}
