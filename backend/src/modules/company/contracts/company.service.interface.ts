import {
  Company,
  CompanyWithData,
  CompanyWithPassword,
} from '#src/modules/company/models/company';
import { CompanyForUpdate } from '#src/modules/company/models/company-for-update';
import { CompanyId } from '#src/modules/company/models/company-id';
import { RegisterData } from '#src/modules/company/models/register-data';

export interface CompanyServiceInterface {
  create(registerData: RegisterData): Promise<Company>;

  findMany(): Promise<Company[]>;

  findOneById(companyID: CompanyId): Promise<CompanyWithData>;

  findOneByEmail(companyEmail: string): Promise<CompanyWithPassword>;

  update(updateCompany: CompanyForUpdate): Promise<Company>;

  remove(companyID: CompanyId): Promise<CompanyId>;
}
