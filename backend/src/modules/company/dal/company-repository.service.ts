import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CompanyEntity } from '#src/database/entities/company.entity';
import { CompanyRepositoryInterface } from '#src/modules/company/contracts/company.repository.interface';
import {
  Company,
  CompanyWithData,
  CompanyWithPassword,
} from '#src/modules/company/models/company';
import { CompanyForUpdate } from '#src/modules/company/models/company-for-update';
import { CompanyId } from '#src/modules/company/models/company-id';
import { RegisterData } from '#src/modules/company/models/register-data';

@Injectable()
export class CompanyRepository implements CompanyRepositoryInterface {
  constructor(
    @InjectRepository(CompanyEntity) private company: Repository<CompanyEntity>
  ) {}
  async create(registerData: RegisterData): Promise<Company> {
    const companyEntityData: Partial<CompanyEntity> = {
      name: registerData.name,
      email: registerData.email,
      password: registerData.password,
      created_at: new Date(),
    };

    const companyEntity: CompanyEntity = await this.company.save(
      companyEntityData
    );

    return {
      id: companyEntity.id as CompanyId,
      email: companyEntity.email,
      name: companyEntity.name,
      created_at: companyEntity.created_at,
      updated_at: companyEntity.updated_at,
    };
  }

  async findMany(): Promise<Company[]> {
    const foundedCompanys = await this.company
      .createQueryBuilder()
      .select()
      .getMany();

    return foundedCompanys.map(company => ({
      id: company.id as CompanyId,
      email: company.email,
      name: company.name,
      created_at: company.created_at,
      updated_at: company.updated_at,
    }));
  }

  async findOneById(companyId: CompanyId): Promise<CompanyWithData> {
    const foundedCompany = await this.company
      .createQueryBuilder('company')
      .select()
      .where('company.id = :companyId', { companyId })
      .getOne();

    if (!foundedCompany) {
      throw new NotFoundException('company not found');
    }

    return {
      id: foundedCompany.id as CompanyId,
      email: foundedCompany.email,
      name: foundedCompany.name,
      type: foundedCompany.type,
      logo: foundedCompany.logo,
      description: foundedCompany.description,
      photos: foundedCompany.photos
        ? foundedCompany.photos.map(p => ({
            id: p.id,
            name: p.name,
            company_id: p.company_id,
          }))
        : [],
      created_at: foundedCompany.created_at,
      updated_at: foundedCompany.updated_at,
    };
  }

  async findOneByEmail(companyEmail: string): Promise<CompanyWithPassword> {
    const foundedCompany = await this.company
      .createQueryBuilder('company')
      .select()
      .where('company.email = :companyEmail', { companyEmail })
      .getOne();

    if (!foundedCompany) {
      throw new NotFoundException('company not found');
    }

    return {
      id: foundedCompany.id as CompanyId,
      email: foundedCompany.email,
      name: foundedCompany.name,
      password: foundedCompany.password,
      created_at: foundedCompany.created_at,
      updated_at: foundedCompany.updated_at,
    };
  }

  async update(updateCompany: CompanyForUpdate): Promise<Company> {
    const { id, ...pauload } = updateCompany;
    const updated = await this.company
      .createQueryBuilder('company')
      .update()
      .set(pauload)
      .where('company.id = :id', { id })
      .returning(['id', 'email', 'company_name', 'created_at', 'updated_at']) // Return the updated company's ID
      .execute();

    const updatedCompany: CompanyEntity = updated.raw[0];

    if (!updatedCompany) {
      throw new NotFoundException('company not found');
    }

    return {
      id: updatedCompany.id as CompanyId,
      email: updatedCompany.email,
      name: updatedCompany.name,
      created_at: updatedCompany.created_at,
      updated_at: updatedCompany.updated_at,
    };
  }

  async remove(companyId: CompanyId): Promise<CompanyId> {
    const deleteResult = await this.company
      .createQueryBuilder('company')
      .delete()
      .where('company.id = :companyID', { companyId })
      .execute();

    if (deleteResult.affected !== 1) {
      throw new NotFoundException('company not found');
    }
    return companyId;
  }
}
