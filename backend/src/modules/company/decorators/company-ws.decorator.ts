import {
  createParamDecorator,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { Socket } from 'socket.io';

import { Company } from '#src/modules/company/models/company';

export const CurrentCompanyWS = createParamDecorator(
  (_data: string, ctx: ExecutionContext): Company => {
    const client: Socket = ctx.switchToWs().getClient<Socket>();

    if (!client.data || !client.data.company) {
      throw new UnauthorizedException('company is not provided');
    }

    return {
      id: client.data.company.id,
      email: client.data.company.email,
      name: client.data.company.name,
      created_at: client.data.company.created_at,
      updated_at: client.data.company.updated_at,
    };
  }
);
