import {
  createParamDecorator,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';

import { ExpressRequest } from '#src/modules/auth/models/express-request';
import { Company } from '#src/modules/company/models/company';

export const CurrentCompany = createParamDecorator(
  (_data: string, ctx: ExecutionContext): Company => {
    const request = ctx.switchToHttp().getRequest<ExpressRequest>();
    if (!request.company) {
      throw new UnauthorizedException('company is not provided');
    }
    return {
      id: request.company.id,
      email: request.company.email,
      name: request.company.name,
      created_at: request.company.created_at,
      updated_at: request.company.updated_at,
    };
  }
);
