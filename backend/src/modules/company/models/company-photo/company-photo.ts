import { CompanyId } from '#src/modules/company/models/company-id';
import { CompanyPhotoId } from '#src/modules/company/models/company-photo/company-photo-id';

export type CompanyPhoto = {
  id: CompanyPhotoId;
  name: string;
  company_id: CompanyId;
};
