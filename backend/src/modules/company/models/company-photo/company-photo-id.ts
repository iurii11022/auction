import { Opaque } from '#src/utils/opaque';

export type CompanyPhotoId = Opaque<string, 'CompanyPhotoId'>;
