import { CompanyId } from '#src/modules/company/models/company-id';
import { CompanyPassword } from '#src/modules/company/models/company-password';
import { CompanyPhoto } from '#src/modules/company/models/company-photo/company-photo';
import { CompanyType } from '#src/modules/company/models/company-type';

export type Company = {
  id: CompanyId;
  email: string;
  name: string;
  description?: string;
  created_at: Date;
  updated_at: Date;
};

export type CompanyWithPassword = Company & { password: CompanyPassword };

export type CompanyWithData = Company & {
  logo: string;
  photos: CompanyPhoto[];
  type: CompanyType;
};
