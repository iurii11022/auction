import { Opaque } from '#src/utils/opaque';

export type CompanyId = Opaque<string, 'CompanyId'>;
