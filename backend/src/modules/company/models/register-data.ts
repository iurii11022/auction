import { CompanyPassword } from '#src/modules/company/models/company-password';

export type RegisterData = {
  name: string;
  email: string;
  password: CompanyPassword;
};
