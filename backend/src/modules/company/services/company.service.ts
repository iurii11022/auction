import { Inject, Injectable } from '@nestjs/common';

import { COMPANY_REPOSITORY } from '#src/modules/company/contracts/company.repository.id';
import { CompanyRepositoryInterface } from '#src/modules/company/contracts/company.repository.interface';
import { CompanyServiceInterface } from '#src/modules/company/contracts/company.service.interface';
import {
  Company,
  CompanyWithData,
  CompanyWithPassword,
} from '#src/modules/company/models/company';
import { CompanyForUpdate } from '#src/modules/company/models/company-for-update';
import { CompanyId } from '#src/modules/company/models/company-id';
import { RegisterData } from '#src/modules/company/models/register-data';
import { hashPassword } from '#src/utils/function/hash-password';

@Injectable()
export class CompanyService implements CompanyServiceInterface {
  constructor(
    @Inject(COMPANY_REPOSITORY) private companyRepo: CompanyRepositoryInterface
  ) {}

  async create(registerData: RegisterData): Promise<Company> {
    const hashedPassword = await hashPassword(registerData.password);
    return await this.companyRepo.create({
      ...registerData,
      password: hashedPassword,
    });
  }

  findMany(): Promise<Company[]> {
    return this.companyRepo.findMany();
  }

  findOneById(companyId: CompanyId): Promise<CompanyWithData> {
    return this.companyRepo.findOneById(companyId);
  }

  findOneByEmail(companyEmail: string): Promise<CompanyWithPassword> {
    return this.companyRepo.findOneByEmail(companyEmail);
  }

  update(updateCompany: CompanyForUpdate): Promise<Company> {
    return this.companyRepo.update(updateCompany);
  }

  remove(companyID: CompanyId): Promise<CompanyId> {
    return this.companyRepo.remove(companyID);
  }
}
