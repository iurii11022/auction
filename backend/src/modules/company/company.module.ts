import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CompanyEntity } from '#src/database/entities/company.entity';
import { CompanyPhotoEntity } from '#src/database/entities/company-photo.entity';
import { COMPANY_REPOSITORY } from '#src/modules/company/contracts/company.repository.id';
import { COMPANY_SERVICE } from '#src/modules/company/contracts/company.servise.id';
import { CompanyRepository } from '#src/modules/company/dal/company-repository.service';
import { CompanyController } from '#src/modules/company/getaway/company.controller';
import { CompanyDtoMapper } from '#src/modules/company/getaway/company.dto.mapper';
import { CompanyPresentationMapper } from '#src/modules/company/getaway/company.presentation.mapper';
import { CompanyService } from '#src/modules/company/services/company.service';

@Module({
  imports: [TypeOrmModule.forFeature([CompanyEntity, CompanyPhotoEntity])],
  controllers: [CompanyController],
  providers: [
    CompanyDtoMapper,
    CompanyPresentationMapper,
    { useClass: CompanyService, provide: COMPANY_SERVICE },
    { useClass: CompanyRepository, provide: COMPANY_REPOSITORY },
  ],
  exports: [COMPANY_SERVICE],
})
export class CompanyModule {}
