import { Injectable } from '@nestjs/common';

import { CompanyId } from '#src/modules/company/models/company-id';
import { removeNullable } from '#src/utils/function/remove-nullable';

import { CompanyForUpdate } from '../models/company-for-update';
import { RegisterData } from '../models/register-data';
import { CreateCompanyDto } from './dto/create-company.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';

@Injectable()
export class CompanyDtoMapper {
  createCompanyDtoToCompanyDto(
    createCompanyDto: CreateCompanyDto
  ): RegisterData {
    return {
      email: createCompanyDto.email,
      name: createCompanyDto.company_name,
      password: createCompanyDto.password,
    };
  }
  updateCompanyDtoToUpdateCompanyDto(
    updateCompanyDto: UpdateCompanyDto,
    companyID: string
  ): CompanyForUpdate {
    const updateCompany = {
      id: companyID as CompanyId,
      email: updateCompanyDto.email,
      companyName: updateCompanyDto.company_name,
      password: updateCompanyDto.password,
    };

    return removeNullable(updateCompany);
  }
}
