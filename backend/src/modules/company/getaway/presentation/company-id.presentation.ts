import { ApiProperty } from '@nestjs/swagger';

export class CompanyIdPresentation {
  @ApiProperty({
    description: 'company',
    example: '38a40528-273e-498d-be75-9fe96a900cb2',
  })
  public company_id!: string;
}
