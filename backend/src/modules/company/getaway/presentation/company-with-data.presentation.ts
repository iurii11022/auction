import { ApiProperty } from '@nestjs/swagger';

import { CompanyPhoto } from '#src/modules/company/models/company-photo/company-photo';
import { CompanyType } from '#src/modules/company/models/company-type';

export class CompanyWithDataPresentation {
  @ApiProperty({
    description: 'company',
    example: '38a40528-273e-498d-be75-9fe96a900cb2',
  })
  public id!: string;

  @ApiProperty({ description: 'company email', example: 'company@gmail.com' })
  public email!: string;

  @ApiProperty({ description: 'name of company', example: 'nic' })
  public name!: string;

  @ApiProperty({ description: 'type of company', example: CompanyType.DEFAULT })
  public type!: CompanyType;

  @ApiProperty({ description: 'type of company', example: CompanyType.DEFAULT })
  public description?: string;

  @ApiProperty({ description: "company's logo" })
  public logo!: string;

  @ApiProperty({ description: "company's photos" })
  public photos!: CompanyPhoto[];

  @ApiProperty()
  public created_at?: Date;

  @ApiProperty()
  public updated_at?: Date;
}
