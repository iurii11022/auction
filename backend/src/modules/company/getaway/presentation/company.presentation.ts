import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CompanyPresentation {
  @ApiProperty({
    description: 'company',
    example: '38a40528-273e-498d-be75-9fe96a900cb2',
  })
  @IsNotEmpty()
  @IsString()
  public id!: string;

  @ApiProperty({ description: 'company email', example: 'company@gmail.com' })
  @IsNotEmpty()
  @IsString()
  public email!: string;

  @ApiProperty({ description: 'name of company', example: 'nic' })
  @IsNotEmpty()
  @IsString()
  public company_name!: string;
}
