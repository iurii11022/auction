import { ApiProperty } from '@nestjs/swagger';

import { CompanyPresentation } from '#src/modules/company/getaway/presentation/company.presentation';

export class CompanysPresentation {
  @ApiProperty({
    description: 'companys',
    type: () => [CompanyPresentation],
  })
  data!: CompanyPresentation[];
}
