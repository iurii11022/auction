import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { AuthGuard } from '#src/modules/auth/guards/auth.guard';
import { CompanyPresentationMapper } from '#src/modules/company/getaway/company.presentation.mapper';
import { UpdateCompanyDto } from '#src/modules/company/getaway/dto/update-company.dto';
import { CompanyPresentation } from '#src/modules/company/getaway/presentation/company.presentation';
import { CompanyIdPresentation } from '#src/modules/company/getaway/presentation/company-id.presentation';
import { CompanyWithDataPresentation } from '#src/modules/company/getaway/presentation/company-with-data.presentation';
import { CompanysPresentation } from '#src/modules/company/getaway/presentation/companys.presentation';
import { Company } from '#src/modules/company/models/company';
import { CompanyId } from '#src/modules/company/models/company-id';

import { CompanyServiceInterface } from '../contracts/company.service.interface';
import { COMPANY_SERVICE } from '../contracts/company.servise.id';
import { CompanyDtoMapper } from './company.dto.mapper';
import { CreateCompanyDto } from './dto/create-company.dto';

@UseGuards(AuthGuard)
@UsePipes(
  new ValidationPipe({
    transform: true,
    whitelist: true,
    forbidNonWhitelisted: true,
  })
)
@ApiTags('company')
@Controller('company')
export class CompanyController {
  constructor(
    @Inject(COMPANY_SERVICE) private companyService: CompanyServiceInterface,
    private dtoMapper: CompanyDtoMapper,
    private presMapper: CompanyPresentationMapper
  ) {}

  @ApiResponse({
    status: 201,
    description: 'create new company and return company id',
    type: CompanyPresentation,
    isArray: false,
  })
  @Post()
  async create(
    @Body() createCompanyReqBody: CreateCompanyDto
  ): Promise<CompanyPresentation> {
    const createCompany =
      this.dtoMapper.createCompanyDtoToCompanyDto(createCompanyReqBody);
    const createdCompany: Company =
      await this.companyService.create(createCompany);
    return this.presMapper.companyToCompanyPresentation(createdCompany);
  }

  @ApiResponse({
    status: 200,
    description: 'find companys and return them',
    type: CompanysPresentation,
    isArray: true,
  })
  @Get()
  async findMany(): Promise<CompanysPresentation> {
    const foundedCompanys = await this.companyService.findMany();
    return this.presMapper.companysToCompanysPresentation(foundedCompanys);
  }

  @ApiResponse({
    status: 200,
    description: 'find one company by id and return it',
    type: CompanyWithDataPresentation,
    isArray: true,
  })
  @Get(':companyId')
  async findOneById(
    @Param('companyId', new ParseUUIDPipe()) companyID: string
  ): Promise<CompanyWithDataPresentation> {
    const foundedCompany = await this.companyService.findOneById(
      companyID as CompanyId
    );
    return this.presMapper.companyToCompanyWithDataPresentation(foundedCompany);
  }

  @ApiResponse({
    status: 200,
    description: 'update company and return company id',
    type: CompanyPresentation,
    isArray: false,
  })
  @Patch(':companyId')
  async update(
    @Param('companyId', new ParseUUIDPipe()) companyID: string,
    @Body() updateCompanyDto: UpdateCompanyDto
  ): Promise<CompanyPresentation> {
    const updateCompany = this.dtoMapper.updateCompanyDtoToUpdateCompanyDto(
      updateCompanyDto,
      companyID
    );
    const updatedCompany = await this.companyService.update(updateCompany);
    return this.presMapper.companyToCompanyPresentation(updatedCompany);
  }

  @ApiResponse({
    status: 200,
    description: 'remove company and return company id',
    type: CompanyIdPresentation,
    isArray: false,
  })
  @Delete(':companyId')
  async remove(
    @Param('companyId', new ParseUUIDPipe()) companyId: string
  ): Promise<CompanyIdPresentation> {
    const removedCompanyId = await this.companyService.remove(
      companyId as CompanyId
    );
    return { company_id: removedCompanyId as string };
  }
}
