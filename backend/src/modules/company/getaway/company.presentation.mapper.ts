import { Injectable } from '@nestjs/common';

import { CompanyPresentation } from '#src/modules/company/getaway/presentation/company.presentation';
import { CompanyWithDataPresentation } from '#src/modules/company/getaway/presentation/company-with-data.presentation';
import { CompanysPresentation } from '#src/modules/company/getaway/presentation/companys.presentation';
import { Company, CompanyWithData } from '#src/modules/company/models/company';

@Injectable()
export class CompanyPresentationMapper {
  companyToCompanyPresentation(company: Company): CompanyPresentation {
    return {
      id: company.id,
      email: company.email,
      company_name: company.name,
    };
  }
  companyToCompanyWithDataPresentation(
    company: CompanyWithData
  ): CompanyWithDataPresentation {
    return {
      id: company.id,
      email: company.email,
      name: company.name,
      description: company.description,
      logo: company.logo,
      type: company.type,
      photos: company.photos,
      created_at: company.created_at,
      updated_at: company.updated_at,
    };
  }

  companysToCompanysPresentation(companys: Company[]): CompanysPresentation {
    return {
      data: companys.map(company => this.companyToCompanyPresentation(company)),
    };
  }
}
