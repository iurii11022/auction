import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class CreateCompanyDto {
  @ApiProperty({ description: 'company email', example: 'company@gmail.com' })
  @IsNotEmpty()
  @IsEmail()
  public email!: string;

  @ApiProperty({ description: 'name of company', example: 'Nic' })
  @IsNotEmpty()
  @IsString()
  @MinLength(2)
  public company_name!: string;

  @ApiProperty({ description: 'company password', example: 'qwerty' })
  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @MaxLength(24)
  public password!: string;
}
