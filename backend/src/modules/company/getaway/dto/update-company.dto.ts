import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class UpdateCompanyDto {
  @ApiProperty({
    description: 'company email',
    example: 'company@gmail.com',
  })
  @IsOptional()
  @IsEmail()
  public email?: string;

  @ApiProperty({
    description: 'name of company',
    example: 'Nic',
  })
  @IsOptional()
  @IsString()
  @MinLength(2)
  public company_name?: string;

  @ApiProperty({
    description: 'company password',
    example: 'qwerty',
  })
  @IsOptional()
  @IsString()
  @MinLength(8)
  @MaxLength(24)
  public password?: string;
}
