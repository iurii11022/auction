import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { dbConfig } from '#src/database/config';
import { AuctionModule } from '#src/modules/auction/auction.module';
import { AuthModule } from '#src/modules/auth/auth.module';
import { AuthMiddleware } from '#src/modules/auth/middlewares/auth.middleware';
import { ChatModule } from '#src/modules/chat/chat.module';
import { CompanyModule } from '#src/modules/company/company.module';
import { PictureModule } from '#src/modules/picture/picture.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(dbConfig),
    CompanyModule,
    ChatModule,
    AuctionModule,
    AuthModule,
    PictureModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude(
        { path: 'auth/register', method: RequestMethod.POST },
        { path: 'auth/login', method: RequestMethod.POST }
      )
      .forRoutes({
        path: '*',
        method: RequestMethod.ALL,
      });
  }
}
