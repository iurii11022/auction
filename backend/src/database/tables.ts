/**
 * This enum holds names of all tables that are created in PostgreSQL.
 */
export enum Tables {
  /** Represents companies participating in the auctions. */
  COMPANY = 'company',

  /** Represents photos belongs to company. */
  COMPANY_PHOTO = 'company_photo',

  /** Represents the auctions being held.*/
  AUCTION = 'auction',

  /** Represents participants in a given auction.*/
  AUCTION_PARTICIPANT = 'auction_participant',

  /** Represents chat rooms associated with an auction.*/
  CHAT = 'chat',

  /** Represents members of a given chat room.*/
  CHAT_MEMBER = 'chat_member',

  /** Represents individual messages within a chat room.*/
  CHAT_MESSAGE = 'chat_message',

  /** Represents replies to chat messages.*/
  CHAT_MESSAGE_REPLY = 'chat_message_reply',

  /** Represents the read status of chat messages for members.*/
  CHAT_MESSAGE_STATUS = 'chat_message_status',

  /** Represents templates for automatic messages.*/
  AUTOMATIC_MESSAGE_TEMPLATE = 'automatic_message_template',

  /** Represents logs of user actions for auditing purposes.*/
  AUDIT_LOG = 'audit_log',
}
