import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { CompanyPhotoEntity } from '#src/database/entities/company-photo.entity';
import { Tables } from '#src/database/tables';
import { CompanyId } from '#src/modules/company/models/company-id';
import { CompanyType } from '#src/modules/company/models/company-type';

@Entity(Tables.COMPANY)
export class CompanyEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: CompanyId;

  @Column({ unique: true })
  name!: string;

  @Column({ type: 'varchar', length: 255, nullable: false, unique: true })
  email!: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  password!: string;

  @Column({ type: 'text', nullable: true })
  logo!: string;

  @Column({ type: 'text', nullable: true })
  description!: string;

  @Column({
    length: 32,
    enum: CompanyType,
    default: CompanyType.DEFAULT,
    nullable: false,
  })
  type!: CompanyType;

  @OneToMany(() => CompanyPhotoEntity, photo => photo.company_id)
  photos!: CompanyPhotoEntity[];

  @CreateDateColumn()
  created_at!: Date;

  @UpdateDateColumn()
  updated_at!: Date;
}
