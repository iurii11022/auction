import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { AuctionParticipantEntity } from '#src/database/entities/auction-participant.entity';
import { ChatEntity } from '#src/database/entities/chat.entity';
import { Tables } from '#src/database/tables';

@Entity(Tables.CHAT_MEMBER)
export class ChatMemberEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ManyToOne(() => ChatEntity)
  @JoinColumn({ name: 'chat_id' })
  chat!: ChatEntity;
  @Column()
  chat_id!: string;

  @ManyToOne(() => AuctionParticipantEntity)
  @JoinColumn({ name: 'auction_participant_id' })
  auction_participant!: AuctionParticipantEntity;
  @Column()
  auction_participant_id!: string;
}
