import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { ChatEntity } from '#src/database/entities/chat.entity';
import { CompanyEntity } from '#src/database/entities/company.entity';
import { Tables } from '#src/database/tables';
import { AuctionId } from '#src/modules/auction/models/auction/auction-id';
import { AuctionStatus } from '#src/modules/auction/models/auction/auction-status';
import { AuctionType } from '#src/modules/auction/models/auction/auction-type';

@Entity(Tables.AUCTION)
export class AuctionEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: AuctionId;

  @Column({
    length: 32,
    enum: AuctionType,
    default: AuctionType.ENGLISH,
    nullable: false,
  })
  type!: AuctionType;

  @Column({ length: 256, nullable: false })
  name!: string;

  @Column({ length: 512, nullable: true })
  description!: string;

  @Column({ nullable: true })
  planned_start!: Date;

  @Column({ nullable: true })
  planned_end!: Date;

  @Column({ length: 256, nullable: true })
  object_name!: string;

  @Column({ type: 'decimal', precision: 8, scale: 2, nullable: true })
  desired_price!: number;

  @Column({ type: 'decimal', precision: 8, scale: 2, nullable: true })
  min_price!: number;

  @Column({ type: 'decimal', precision: 8, scale: 2, nullable: true })
  max_price!: number;

  @Column({ type: 'decimal', precision: 8, scale: 2, nullable: true })
  current_price!: number;

  @Column({ length: 512, nullable: true })
  object_info!: string;

  @Column({ type: 'text', nullable: true })
  main_photo!: string;

  @Column({ type: 'text', nullable: true })
  object_photo_1!: string;

  @Column({ type: 'text', nullable: true })
  object_photo_2!: string;

  @Column({ type: 'text', nullable: true })
  additional_info!: string;

  @Column({
    length: 32,
    enum: AuctionStatus,
    default: AuctionStatus.CREATED,
    nullable: false,
  })
  status!: AuctionStatus;

  @CreateDateColumn()
  created_at!: Date;

  @Column({ nullable: true })
  started_at!: Date;

  @Column({ nullable: true })
  completed_at!: Date;

  @ManyToOne(() => CompanyEntity)
  @JoinColumn()
  winner!: CompanyEntity;

  @OneToMany(() => ChatEntity, chat => chat.auction)
  chats!: ChatEntity[];

  @ManyToOne(() => CompanyEntity)
  @JoinColumn({ name: 'created_by_id' })
  created_by!: CompanyEntity;

  @Column({ nullable: true })
  created_by_id!: string;
}
