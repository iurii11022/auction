import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { ChatMemberEntity } from '#src/database/entities/chat-member.entity';
import { ChatMessageEntity } from '#src/database/entities/chat-message.entity';
import { Tables } from '#src/database/tables';

@Entity(Tables.CHAT_MESSAGE_STATUS)
export class ChatMessageStatusEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ManyToOne(() => ChatMessageEntity)
  @JoinColumn()
  message!: ChatMessageEntity;

  @ManyToOne(() => ChatMemberEntity)
  @JoinColumn()
  chat_member!: ChatMemberEntity;

  @Column('timestamp', { nullable: true })
  read_at!: Date;
}
