import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { AuctionEntity } from '#src/database/entities/auction.entity';
import { CompanyEntity } from '#src/database/entities/company.entity';
import { Tables } from '#src/database/tables';
import { AuctionParticipantRole } from '#src/modules/auction/models/auction-participant/auction-participant-role';

@Entity(Tables.AUCTION_PARTICIPANT)
export class AuctionParticipantEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ManyToOne(() => AuctionEntity)
  @JoinColumn({ name: 'auction_id' })
  auction!: AuctionEntity;
  @Column()
  auction_id!: string;

  @ManyToOne(() => CompanyEntity)
  @JoinColumn({ name: 'company_id' })
  company!: CompanyEntity;
  @Column()
  company_id!: string;

  @Column({ length: 512, nullable: false })
  company_alias!: string;

  @Column({
    enum: AuctionParticipantRole,
    length: 32,
    nullable: false,
  })
  role!: AuctionParticipantRole;

  @Column('timestamp', { nullable: true })
  last_seen!: Date;
}
