import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { CompanyEntity } from '#src/database/entities/company.entity';
import { Tables } from '#src/database/tables';
import { CompanyId } from '#src/modules/company/models/company-id';
import { CompanyPhotoId } from '#src/modules/company/models/company-photo/company-photo-id';

@Entity(Tables.COMPANY_PHOTO)
export class CompanyPhotoEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: CompanyPhotoId;

  @Column({ unique: true })
  name!: string;

  @ManyToOne(() => CompanyEntity)
  @JoinColumn({ name: 'company_id' })
  created_by!: CompanyEntity;
  @Column()
  company_id!: CompanyId;
}
