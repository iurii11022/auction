import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { AuctionEntity } from '#src/database/entities/auction.entity';
import { ChatMemberEntity } from '#src/database/entities/chat-member.entity';
import { ChatMessageEntity } from '#src/database/entities/chat-message.entity';
import { Tables } from '#src/database/tables';
import { ChatType } from '#src/modules/chat/models/chat/chat-type';

@Entity(Tables.CHAT)
export class ChatEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column({ length: 255, nullable: false })
  name!: string;

  @Column({ length: 32, nullable: false, enum: ChatType })
  type!: ChatType;

  @OneToMany(() => ChatMemberEntity, chatMember => chatMember.chat)
  chat_members!: ChatMemberEntity[];

  @OneToMany(() => ChatMessageEntity, chatMessage => chatMessage.chat)
  chat_messages!: ChatMessageEntity[];

  @ManyToOne(() => AuctionEntity)
  @JoinColumn({ name: 'auction_id' })
  auction!: AuctionEntity;
  @Column()
  auction_id!: string;
}
