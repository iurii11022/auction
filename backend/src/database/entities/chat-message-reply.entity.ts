import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { ChatMessageEntity } from '#src/database/entities/chat-message.entity';
import { Tables } from '#src/database/tables';

@Entity(Tables.CHAT_MESSAGE_REPLY)
export class ChatMessageReplyEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ManyToOne(() => ChatMessageEntity)
  @JoinColumn({ name: 'chat_message_id' })
  chat_message!: ChatMessageEntity;
  @Column()
  chat_message_id!: string;

  @ManyToOne(() => ChatMessageEntity)
  @JoinColumn({ name: 'reply_to_chat_message_id' })
  reply_to_message!: ChatMessageEntity;
  @Column()
  reply_to_message_id!: string;
}
