import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { ChatEntity } from '#src/database/entities/chat.entity';
import { ChatMemberEntity } from '#src/database/entities/chat-member.entity';
import { Tables } from '#src/database/tables';
import { ChatMessageType } from '#src/modules/chat/models/chat-message/chat-message-type';

@Entity(Tables.CHAT_MESSAGE)
export class ChatMessageEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ManyToOne(() => ChatEntity)
  @JoinColumn({ name: 'chat_id' })
  chat!: ChatEntity;
  @Column()
  chat_id!: string;

  @CreateDateColumn()
  created_at!: Date;

  @ManyToOne(() => ChatMemberEntity)
  @JoinColumn({ name: 'author_id' })
  author!: ChatMemberEntity;
  @Column()
  author_id!: string;

  @Column('text', { nullable: false })
  text_content!: string;

  @Column({ type: 'decimal', precision: 5, scale: 2, nullable: true })
  value!: number;

  @Column({
    length: 32,
    nullable: false,
    enum: ChatMessageType,
    default: ChatMessageType.MESSAGE,
  })
  type!: ChatMessageType;
}
