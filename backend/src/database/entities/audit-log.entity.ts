import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Tables } from '#src/database/tables';

@Entity(Tables.AUDIT_LOG)
export class AuditLogEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @CreateDateColumn()
  timestamp!: Date;

  @Column({ length: 32, nullable: true })
  user_type!: string;

  @Column({ length: 256, nullable: true })
  user_id!: string;

  @Column({ length: 256, nullable: true })
  action_type!: string;

  @Column('text', { nullable: true })
  action_data!: string;
}
