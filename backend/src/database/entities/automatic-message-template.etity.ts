import { Column, Entity } from 'typeorm';

import { Tables } from '#src/database/tables';

@Entity(Tables.AUTOMATIC_MESSAGE_TEMPLATE)
export class AutomaticMessageTemplateEntity {
  @Column({ length: 128, primary: true })
  message_type!: string;

  @Column('text', { nullable: false })
  description!: string;

  @Column('text', { nullable: false })
  template_text!: string;
}
