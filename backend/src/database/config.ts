import { DataSourceOptions } from 'typeorm/data-source/DataSourceOptions';

import { AuctionEntity } from '#src/database/entities/auction.entity';
import { AuctionParticipantEntity } from '#src/database/entities/auction-participant.entity';
import { AuditLogEntity } from '#src/database/entities/audit-log.entity';
import { AutomaticMessageTemplateEntity } from '#src/database/entities/automatic-message-template.etity';
import { ChatEntity } from '#src/database/entities/chat.entity';
import { ChatMemberEntity } from '#src/database/entities/chat-member.entity';
import { ChatMessageEntity } from '#src/database/entities/chat-message.entity';
import { ChatMessageReplyEntity } from '#src/database/entities/chat-message-reply.entity';
import { ChatMessageStatusEntity } from '#src/database/entities/chat-message-status.entity';
import { CompanyEntity } from '#src/database/entities/company.entity';
import { CompanyPhotoEntity } from '#src/database/entities/company-photo.entity';

export const dbConfig: DataSourceOptions = {
  type: (process.env.DB_CONNECTION as 'postgres') ?? 'postgres',
  username: process.env.DB_USERNAME ?? 'petrobras-chat-db',
  password: process.env.DB_PASSWORD ?? 'petrobras23123123',
  database: process.env.DB_DATABASE ?? 'petrobras-chat-db',
  host: process.env.DB_HOST ?? 'localhost',
  port: Number.isFinite(Number(process.env.DB_PORT))
    ? Number(process.env.DB_PORT)
    : 5432,
  entities: [
    CompanyEntity,
    CompanyPhotoEntity,
    AuctionEntity,
    AuctionParticipantEntity,
    AuditLogEntity,
    AutomaticMessageTemplateEntity,
    ChatEntity,
    ChatMemberEntity,
    ChatMessageEntity,
    ChatMessageReplyEntity,
    ChatMessageStatusEntity,
  ],
  synchronize: true,
  logging: true,
};
