import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { AppModule } from '#src/app.module';
import { getAppConfig } from '#src/config/app-config';

async function bootstrap() {
  const appConfig = getAppConfig();

  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix(appConfig.globalPrefix);
  app.enableCors({
    origin: 'http://localhost:4200',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
  });

  const swaggerUrl = `${appConfig.globalPrefix}/swagger`;
  const config = new DocumentBuilder()
    .setTitle(`petrobras-chat`)
    .setDescription('api service')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup(swaggerUrl, app, document);

  await app.listen(appConfig.port, () => {
    console.log(`app has been started on port ${appConfig.port}`);
    console.log(`http://localhost:${appConfig.port}/${appConfig.globalPrefix}`);
  });
}

bootstrap();
