### Auction Application Setup Guide

#### Environment Configuration

Copy and Rename Configuration File
```
cp .env.example .env
```


### Launching the Application
After setting up your environment variables:

Starting Services with Docker

Navigate to the directory containing your docker-compose.yml file and initiate the services using the following command:

```
docker-compose up --build
```

Accessing the Web Interface

Once all services are running, you can access the application's web interface via your browser:

http://websystem-docker.local



### Optional AWS Configuration for Picture Uploading

If you'd like to enable the picture uploading feature, you'll need to setup the corresponding AWS parameters in the .env file:

#### AWS_ACCESS_KEY_ID: Your AWS access key ID.
#### AWS_SECRET_ACCESS_KEY: Your AWS secret access key.
#### AWS_S3_BUCKET_NAME: The S3 bucket's name where the images will be stored.
#### AWS_S3_BUCKET_FOLDER_NAME: A designated folder within the S3 bucket for organized storage.

Ensure that your AWS account has the necessary permissions for S3 bucket operations.
