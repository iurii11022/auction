import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, take } from 'rxjs';
import {
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
} from '@angular/router';
import { currentAccount } from './modules/auth/state-management/auth.selectors';
import {
  logoutAction,
  refreshSessionAction,
} from './modules/auth/state-management/auth.actions';
import { User } from '#src/app/modules/auth/models/user';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  loading = true;
  currentAccount$!: Observable<User | null>;

  constructor(
    private router: Router,
    private store: Store
  ) {}

  ngOnInit() {
    this.store.dispatch(refreshSessionAction());

    this.router.events.subscribe(event => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.loading = true;
          break;
        }
        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });

    this.currentAccount$ = this.store.pipe(select(currentAccount));
  }

  logout() {
    this.store.dispatch(logoutAction());
  }

  toCompanyPage() {
    this.currentAccount$
      .pipe(
        take(1),
        filter(user => !!user)
      )
      .subscribe(user => {
        this.router.navigate(['/company', user.id]);
      });
  }
}
