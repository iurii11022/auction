import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule, Routes } from '@angular/router';
import { MatExpansionModule } from '@angular/material/expansion';

import { ChatComponent } from '#src/app/modules/chat/components/chat/chat.component';
import { CustomSocket } from '#src/app/modules/chat/api-websocket-client/socket/custom-socket';
import { ChatService } from '#src/app/modules/chat/services/chat.service';
import { ChatSocketClient } from '#src/app/modules/chat/api-websocket-client/chat.socket.client';
import { SearchModule } from '#src/app/core-modules/search/search.module';
import { SelectorModule } from '#src/app/core-modules/selector/selector.module';
import { ChatPageComponent } from '#src/app/modules/chat/components/chat-page/chat-page.component';
import { ChatDetailsComponent } from '#src/app/modules/chat/components/chat-details/chat-details.component';
import { ChatMessageComponent } from '#src/app/modules/chat/components/chat-message/chat-message.component';
import { ChatMessageNewMemberComponent } from '#src/app/modules/chat/components/chat-message-new-member/chat-message-new-member.component';
import { ChatMessageStartComponent } from '#src/app/modules/chat/components/chat-message-start/chat-message-start.component';
import { ChatMessageStopComponent } from '#src/app/modules/chat/components/chat-message-stop/chat-message-stop.component';
import { ChatMessageBetComponent } from '#src/app/modules/chat/components/chat-message-bet/chat-message-bet.component';

export const chatRoutes: Routes = [
  {
    path: '',
    component: ChatPageComponent,
  },
];

@NgModule({
  imports: [
    SelectorModule,
    SearchModule,
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatTabsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    ReactiveFormsModule,
    MatExpansionModule,
    RouterModule.forChild(chatRoutes),
  ],
  declarations: [
    ChatComponent,
    ChatPageComponent,
    ChatDetailsComponent,
    ChatMessageComponent,
    ChatMessageNewMemberComponent,
    ChatMessageStartComponent,
    ChatMessageStopComponent,
    ChatMessageBetComponent,
  ],
  exports: [ChatComponent],
  providers: [ChatSocketClient, ChatService, CustomSocket],
})
export class ChatModule {}
