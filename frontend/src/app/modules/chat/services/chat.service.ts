import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ChatSocketClient } from '#src/app/modules/chat/api-websocket-client/chat.socket.client';
import { ChatWithData } from '#src/app/modules/chat/models/chat/chat-with-data';
import { ChatMessageToCreate } from '#src/app/modules/chat/models/chat-message/chat-message-to-create';
import { JoinToChat } from '#src/app/modules/chat/models/chat-message/join-to-chat';
import { ChatMessageResponce } from '#src/app/modules/chat/models/chat-message/chat-message-responce';
import { ChatMessageStart } from '#src/app/modules/chat/models/chat-message/chat-message-start';
import { ChatMessageStop } from '#src/app/modules/chat/models/chat-message/chat-message-stop';
import { ChatMessageBet } from '#src/app/modules/chat/models/chat-message/chat-message-bet';

@Injectable()
export class ChatService {
  constructor(private socketClient: ChatSocketClient) {}

  socketDisconnect(): void {
    return this.socketClient.socketDisconnect();
  }

  getChatById(chatId: string): void {
    return this.socketClient.getChatById(chatId);
  }

  chatWithData(): Observable<ChatWithData> {
    return this.socketClient.chatWithData();
  }

  sendMessage(message: ChatMessageToCreate) {
    return this.socketClient.sendMessage(message);
  }

  sendBetMessage(message: ChatMessageBet) {
    return this.socketClient.sendBetMessage(message);
  }

  startAuctionMessage(message: ChatMessageStart) {
    return this.socketClient.startAuctionMessage(message);
  }

  stopAuctionMessage(message: ChatMessageStop) {
    return this.socketClient.stopAuctionMessage(message);
  }

  sendJoinToChatRequest(message: JoinToChat) {
    return this.socketClient.sendJoinToChatRequest(message);
  }

  message(): Observable<ChatMessageResponce> {
    return this.socketClient.message();
  }
}
