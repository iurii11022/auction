import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Component({
  selector: 'app-chat-message-stop',
  templateUrl: './chat-message-stop.component.html',
  styleUrls: ['./chat-message-stop.component.scss'],
})
export class ChatMessageStopComponent implements OnChanges {
  public form!: UntypedFormGroup;

  @Input() author: string;
  @Input() text_value: string;
  @Input() created_at: string;

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    console.log(this.author, 'author');
  }
}
