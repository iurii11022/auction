import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Component({
  selector: 'app-chat-message',
  templateUrl: './chat-message.component.html',
  styleUrls: ['./chat-message.component.scss'],
})
export class ChatMessageComponent implements OnChanges {
  public form!: UntypedFormGroup;

  @Input() author: string;
  @Input() text_value: string;
  @Input() author_id: string;
  @Input() current_participant: string;

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    console.log(this.author, 'author');
    console.log(this.author_id, 'author_id');
    console.log(this.current_participant, 'current_participant');
  }
}
