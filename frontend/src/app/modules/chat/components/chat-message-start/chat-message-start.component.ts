import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Component({
  selector: 'app-chat-message-start',
  templateUrl: './chat-message-start.component.html',
  styleUrls: ['./chat-message-start.component.scss'],
})
export class ChatMessageStartComponent implements OnChanges {
  public form!: UntypedFormGroup;

  @Input() author: string;
  @Input() text_value: string;
  @Input() created_at: string;

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    console.log(this.author, 'author');
  }
}
