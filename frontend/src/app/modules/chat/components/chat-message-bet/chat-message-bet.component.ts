import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Component({
  selector: 'app-chat-message-bet',
  templateUrl: './chat-message-bet.component.html',
  styleUrls: ['./chat-message-bet.component.scss'],
})
export class ChatMessageBetComponent implements OnChanges {
  public form!: UntypedFormGroup;

  @Input() author: string;
  @Input() text_value: string;
  @Input() created_at: string;

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {}
}
