import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { ChatWithData } from '#src/app/modules/chat/models/chat/chat-with-data';
import { ChatMessageStart } from '#src/app/modules/chat/models/chat-message/chat-message-start';
import { ChatMessageType } from '#src/app/modules/chat/models/chat-message/chat-message-type';
import { AuctionStatus } from '#src/app/modules/auctions/models/auction-status';
import { ChatMessageResponce } from '#src/app/modules/chat/models/chat-message/chat-message-responce';
import { ChatMessageStop } from '#src/app/modules/chat/models/chat-message/chat-message-stop';

@Component({
  selector: 'app-chat-details',
  templateUrl: './chat-details.component.html',
  styleUrls: ['./chat-details.component.scss'],
})
export class ChatDetailsComponent implements OnChanges {
  @Output() startAuctionMessage = new EventEmitter<ChatMessageStart>();
  @Output() stopAuctionMessage = new EventEmitter<ChatMessageStop>();
  @Input() chat: ChatWithData;
  @Input() newMessage: ChatMessageResponce;

  ngOnChanges(changes: SimpleChanges) {
    if (changes['newMessage'] && changes['newMessage'].currentValue) {
      if (
        changes['newMessage'].currentValue.type ===
        ChatMessageType.START_AUCTION
      ) {
        this.chat.auction.status = AuctionStatus.STARTED;
        this.chat.auction.started_at = new Date();
      }

      if (
        changes['newMessage'].currentValue.type === ChatMessageType.STOP_AUCTION
      ) {
        this.chat.auction.status = AuctionStatus.FINISHED;
        this.chat.auction.completed_at = new Date();
      }
    }
  }

  startAuction() {
    this.startAuctionMessage.emit({
      chat_id: this.chat.id,
      author_id: this.chat.current_chat_member.id,
      auction_id: this.chat.auction.id,
    });
  }

  stopAuction() {
    this.stopAuctionMessage.emit({
      chat_id: this.chat.id,
      author_id: this.chat.current_chat_member.id,
      auction_id: this.chat.auction.id,
    });
  }
}
