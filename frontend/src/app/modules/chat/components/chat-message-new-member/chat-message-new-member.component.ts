import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Component({
  selector: 'app-chat-message-new-member',
  templateUrl: './chat-message-new-member.component.html',
  styleUrls: ['./chat-message-new-member.component.scss'],
})
export class ChatMessageNewMemberComponent implements OnChanges {
  public form!: UntypedFormGroup;

  @Input() author: string;
  @Input() text_value: string;
  @Input() created_at: string;

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    console.log(this.author, 'author');
  }
}
