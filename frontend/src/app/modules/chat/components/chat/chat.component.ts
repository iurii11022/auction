import {
  AfterViewChecked,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { ChatWithData } from '#src/app/modules/chat/models/chat/chat-with-data';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';

import { ChatMessageToCreate } from '#src/app/modules/chat/models/chat-message/chat-message-to-create';
import { ChatMemberId } from '#src/app/modules/chat/models/chat-member/chat-member-id';
import { JoinToChat } from '#src/app/modules/chat/models/chat-message/join-to-chat';
import { ChatMessageResponce } from '#src/app/modules/chat/models/chat-message/chat-message-responce';
import { ChatMessageType } from '#src/app/modules/chat/models/chat-message/chat-message-type';
import { ChatMessageBet } from '#src/app/modules/chat/models/chat-message/chat-message-bet';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit, OnChanges, AfterViewChecked {
  public form!: UntypedFormGroup;
  public formBet!: UntypedFormGroup;

  @Input() chat: ChatWithData;
  @Input() currentChatMemberId: ChatMemberId;
  @Input() chatMessages: ChatMessageResponce[];
  @Input() newMessage: ChatMessageResponce;

  @Output() createdMessage = new EventEmitter<ChatMessageToCreate>();
  @Output() joinToChat = new EventEmitter<JoinToChat>();
  @Output() newBetMessage = new EventEmitter<ChatMessageBet>();

  @ViewChild('messagesContainer') private messagesContainer: ElementRef;

  constructor(private fb: UntypedFormBuilder) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      message: ['hi', [Validators.required]],
    });

    this.formBet = this.fb.group({
      bet: ['100', [Validators.required]],
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(this.newMessage, 'newMessage');
    if (changes['newMessage'] && changes['newMessage'].currentValue) {
      if (
        changes['newMessage'].currentValue.type ===
          ChatMessageType.NEW_MEMBER &&
        !this.currentChatMemberId
      ) {
        this.currentChatMemberId =
          changes['newMessage'].currentValue.chat_member_id;
      }
      this.chatMessages.push(changes['newMessage'].currentValue);
    }
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  onSendMessage(): void {
    this.createdMessage.emit({
      chat_id: this.chat.id,
      author_id: this.currentChatMemberId,
      text_content: this.form.value.message,
    });
  }

  onSendBetMessage(): void {
    this.newBetMessage.emit({
      chat_id: this.chat.id,
      author_id: this.currentChatMemberId,
      text_content: this.formBet.value.bet,
      auction_id: this.chat.auction.id,
    });
  }

  onJoinToChat(): void {
    this.joinToChat.emit({
      chat_id: this.chat.id,
    });
  }

  private scrollToBottom(): void {
    try {
      this.messagesContainer.nativeElement.scrollTop =
        this.messagesContainer.nativeElement.scrollHeight;
    } catch (err) {}
  }
}
