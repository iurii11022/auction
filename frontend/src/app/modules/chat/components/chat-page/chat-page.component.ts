import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UntypedFormGroup } from '@angular/forms';
import { ChatId } from '#src/app/modules/chat/models/chat/chat-id';
import { Observable, share } from 'rxjs';
import { ChatWithData } from '#src/app/modules/chat/models/chat/chat-with-data';
import { ChatService } from '#src/app/modules/chat/services/chat.service';
import { tap } from 'rxjs/operators';
import { ChatMessageToCreate } from '#src/app/modules/chat/models/chat-message/chat-message-to-create';
import { JoinToChat } from '#src/app/modules/chat/models/chat-message/join-to-chat';
import { ChatMessageResponce } from '#src/app/modules/chat/models/chat-message/chat-message-responce';
import { ChatMessageStart } from '#src/app/modules/chat/models/chat-message/chat-message-start';
import { ChatMessageStop } from '#src/app/modules/chat/models/chat-message/chat-message-stop';
import { ChatMessageBet } from '#src/app/modules/chat/models/chat-message/chat-message-bet';

@Component({
  selector: 'app-chat-page',
  templateUrl: './chat-page.component.html',
  styleUrls: ['./chat-page.component.css'],
})
export class ChatPageComponent implements OnInit, OnDestroy {
  public chatId: ChatId;
  public form!: UntypedFormGroup;
  public chat$: Observable<ChatWithData>;
  public newMessage$: Observable<ChatMessageResponce>;

  constructor(private route: ActivatedRoute, private chatService: ChatService) {
    this.chatId = this.route.snapshot.paramMap.get('chatId') as ChatId;
    this.newMessage$ = this.chatService.message();
    this.chat$ = this.chatService.chatWithData().pipe(
      tap(c => {
        console.log(c, 'chat');
      }),
      share()
    );
  }

  ngOnInit(): void {
    this.chatService.getChatById(this.chatId);
  }

  ngOnDestroy() {
    this.chatService.socketDisconnect();
  }

  onNewMessage(message: ChatMessageToCreate) {
    this.chatService.sendMessage(message);
  }

  onBetMessage(message: ChatMessageBet) {
    this.chatService.sendBetMessage(message);
  }

  onStartAuctionMessage(message: ChatMessageStart) {
    this.chatService.startAuctionMessage(message);
  }

  onStopAuctionMessage(message: ChatMessageStop) {
    this.chatService.stopAuctionMessage(message);
  }

  onJoinToChat(message: JoinToChat) {
    this.chatService.sendJoinToChatRequest(message);
  }
}
