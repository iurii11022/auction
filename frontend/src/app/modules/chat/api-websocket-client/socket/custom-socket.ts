import { Injectable } from '@angular/core';
import { Socket, SocketIoConfig } from 'ngx-socket-io';
import { JwtStorage } from '#src/app/core-modules/jwt/Jwt.storage';
import { environment } from '#src/environments/environment';

const config: SocketIoConfig = {
  url: environment.backendWsUrl,
  options: {
    extraHeaders: {
      Authorization: JwtStorage.getToken('accessToken'),
    },
  },
};

@Injectable()
export class CustomSocket extends Socket {
  constructor() {
    super(config);
  }
}
