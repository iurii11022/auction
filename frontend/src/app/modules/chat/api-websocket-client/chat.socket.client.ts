import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CustomSocket } from '#src/app/modules/chat/api-websocket-client/socket/custom-socket';
import { ChatWithData } from '#src/app/modules/chat/models/chat/chat-with-data';
import { ChatMessageToCreate } from '#src/app/modules/chat/models/chat-message/chat-message-to-create';
import { JoinToChat } from '#src/app/modules/chat/models/chat-message/join-to-chat';
import { ChatMessageResponce } from '#src/app/modules/chat/models/chat-message/chat-message-responce';
import { ChatMessageStart } from '#src/app/modules/chat/models/chat-message/chat-message-start';
import { ChatMessageStop } from '#src/app/modules/chat/models/chat-message/chat-message-stop';

enum event {
  GET_CHAT = 'GET_CHAT',
  CHAT_WITH_DATA = 'CHAT_WITH_DATA',

  SEND_MESSAGE = 'SEND_MESSAGE',

  SEND_BET_MESSAGE = 'SEND_BET_MESSAGE',

  START_AUCTION = 'START_AUCTION',

  JOIN_TO_CHAT = 'JOIN_TO_CHAT',

  STOP_AUCTION = 'STOP_AUCTION',

  MESSAGE = 'MESSAGE',
}

@Injectable()
export class ChatSocketClient {
  constructor(private socket: CustomSocket) {}

  socketDisconnect(): void {
    console.log('disconnect');
    return this.socket.disconnect();
  }

  getChatById(chatId: string): void {
    console.log('connect');
    this.socket.connect();
    return this.socket.emit(event.GET_CHAT, chatId);
  }

  chatWithData(): Observable<ChatWithData> {
    return this.socket.fromEvent<ChatWithData>(event.CHAT_WITH_DATA);
  }

  sendMessage(message: ChatMessageToCreate): void {
    return this.socket.emit(event.SEND_MESSAGE, message);
  }

  sendBetMessage(message: ChatMessageToCreate): void {
    return this.socket.emit(event.SEND_BET_MESSAGE, message);
  }

  startAuctionMessage(message: ChatMessageStart): void {
    return this.socket.emit(event.START_AUCTION, message);
  }

  stopAuctionMessage(message: ChatMessageStop): void {
    return this.socket.emit(event.STOP_AUCTION, message);
  }

  sendJoinToChatRequest(message: JoinToChat): void {
    return this.socket.emit(event.JOIN_TO_CHAT, message);
  }

  message(): Observable<ChatMessageResponce> {
    return this.socket.fromEvent<ChatMessageResponce>(event.MESSAGE);
  }
}
