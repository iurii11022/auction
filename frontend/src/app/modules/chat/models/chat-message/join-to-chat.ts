import { ChatId } from '#src/app/modules/chat/models/chat/chat-id';

export type JoinToChat = {
  chat_id: ChatId;
};
