import { ChatId } from '#src/app/modules/chat/models/chat/chat-id';
import { ChatMemberId } from '#src/app/modules/chat/models/chat-member/chat-member-id';
import { AuctionId } from '#src/app/modules/auctions/models/auction-id';

export type ChatMessageBet = {
  chat_id: ChatId;
  author_id: ChatMemberId;
  text_content: string;
  auction_id: AuctionId;
};
