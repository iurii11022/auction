import { Opaque } from '#src/utils/types/opaque';

export type ChatMessageId = Opaque<string, 'ChatMessageId'>;
