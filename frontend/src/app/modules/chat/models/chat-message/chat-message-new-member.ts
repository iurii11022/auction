import { CompanyId } from '#src/app/modules/company/models/company-id';
import { ChatMessageType } from '#src/app/modules/chat/models/chat-message/chat-message-type';
import { ChatId } from '#src/app/modules/chat/models/chat/chat-id';
import { ChatMemberId } from '#src/app/modules/chat/models/chat-member/chat-member-id';

export type ChatMessageNewMember = {
  id: CompanyId;
  name: string;
  text_content: string;
  message_author: string;
  chat_id: ChatId;
  created_at: Date;
  type: ChatMessageType.NEW_MEMBER;
  chat_member_id: ChatMemberId;
};
