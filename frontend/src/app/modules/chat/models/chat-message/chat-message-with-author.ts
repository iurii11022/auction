import { ChatMessageId } from '#src/app/modules/chat/models/chat-message/chat-message-id';
import { ChatId } from '#src/app/modules/chat/models/chat/chat-id';
import { ChatMessageType } from '#src/app/modules/chat/models/chat-message/chat-message-type';

export type ChatMessageWithAuthor = {
  id: ChatMessageId;
  chat_id: ChatId;
  message_author: string;
  message_author_id: string;
  text_content: string;
  created_at: Date;
  type: ChatMessageType.MESSAGE;
};
