export enum ChatMessageType {
  MESSAGE = 'message',
  NEW_MEMBER = 'new member',
  START_AUCTION = 'start auction',
  STOP_AUCTION = 'stop auction',
}
