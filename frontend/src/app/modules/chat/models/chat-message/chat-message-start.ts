import { ChatId } from '#src/app/modules/chat/models/chat/chat-id';
import { ChatMemberId } from '#src/app/modules/chat/models/chat-member/chat-member-id';
import { AuctionId } from '#src/app/modules/auctions/models/auction-id';

export type ChatMessageStart = {
  chat_id: ChatId;
  author_id: ChatMemberId;
  auction_id: AuctionId;
};
