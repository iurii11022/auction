import { ChatMessageWithAuthor } from '#src/app/modules/chat/models/chat-message/chat-message-with-author';
import { ChatMessageNewMember } from '#src/app/modules/chat/models/chat-message/chat-message-new-member';

export type ChatMessageResponce = ChatMessageWithAuthor | ChatMessageNewMember;
