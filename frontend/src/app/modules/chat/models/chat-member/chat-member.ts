import { ChatMemberId } from '#src/app/modules/chat/models/chat-member/chat-member-id';
import { Chat } from '#src/app/modules/chat/models/chat/chat';
import { ChatId } from '#src/app/modules/chat/models/chat/chat-id';
import { AuctionParticipant } from '#src/app/modules/auctions/models/auction-participant';

export type ChatMember = {
  id: ChatMemberId;
  chat: Chat | ChatId;
  auction_participant: AuctionParticipant;
};
