import { Opaque } from '#src/utils/types/opaque';

export type ChatMemberId = Opaque<string, 'ChatMemberId'>;
