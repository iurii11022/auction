import { ChatMemberId } from '#src/app/modules/chat/models/chat-member/chat-member-id';
import { AuctionParticipantId } from '#src/app/modules/auctions/models/auction-participant-id';
import { ChatId } from '#src/app/modules/chat/models/chat/chat-id';

export type ChatMemberToCreate = {
  id: ChatMemberId;
  chat_id: ChatId;
  auction_participant_d: AuctionParticipantId;
};
