import { Opaque } from '#src/utils/types/opaque';

export type ChatId = Opaque<string, 'ChatId'>;
