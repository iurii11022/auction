import { ChatId } from '#src/app/modules/chat/models/chat/chat-id';
import { ChatMember } from '#src/app/modules/chat/models/chat-member/chat-member';
import { ChatType } from '#src/app/modules/chat/models/chat/chat-type';
import { Auction } from '#src/app/modules/auctions/models/auction';
import { ChatMessageResponce } from '#src/app/modules/chat/models/chat-message/chat-message-responce';

export type ChatWithData = {
  id: ChatId;
  name: string;
  auction: Auction;
  type: ChatType;
  chat_members: ChatMember[];
  chat_messages: ChatMessageResponce[];
  current_chat_member: ChatMember;
  is_auction_owner: boolean;
};
