export enum ChatType {
  PUBLIC = 'public',
  PRIVATE = 'private',
}
