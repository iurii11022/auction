import { AuctionId } from '#src/app/modules/auctions/models/auction-id';
import { ChatType } from '#src/app/modules/chat/models/chat/chat-type';

export type ChatToCreate = {
  name: string;
  type: ChatType;
  auction_id: AuctionId;
};
