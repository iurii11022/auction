import { ChatType } from '#src/app/modules/chat/models/chat/chat-type';
import { ChatId } from '#src/app/modules/chat/models/chat/chat-id';

export type Chat = {
  id: ChatId;
  name: string;
  type: ChatType;
  auction: string;
};
