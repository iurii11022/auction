import { UserWithTokens } from '#src/app/modules/auth/models/user-with-tokens';
import { AuthResponse } from '#src/app/modules/auth/api-http-client/models/auth.response';

export function authResponseToUserWithTokens(
  response: AuthResponse
): UserWithTokens {
  return {
    id: response.id,
    name: response.name,
    email: response.email,
    accessToken: response.access_token,
    refreshToken: response.refresh_token,
  };
}
