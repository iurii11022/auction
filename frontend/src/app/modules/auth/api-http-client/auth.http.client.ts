import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '#src/environments/environment';
import { UserDataForRegister } from '#src/app/modules/auth/models/user-data-for-register';
import { map } from 'rxjs/operators';
import { UserWithTokens } from '#src/app/modules/auth/models/user-with-tokens';
import { authResponseToUserWithTokens } from '#src/app/modules/auth/api-http-client/auth.mapper';
import { AuthResponse } from '#src/app/modules/auth/api-http-client/models/auth.response';
import { UserDataForLogIn } from '#src/app/modules/auth/models/user-data-for-log-in';
import { RegisterRequest } from '#src/app/modules/auth/api-http-client/models/register.request';
import { LogInRequest } from '#src/app/modules/auth/api-http-client/models/log-in.request';

const apiUrls = {
  REGISTER: `${environment.backendApiUrl}/auth/register`,
  LOGIN: `${environment.backendApiUrl}/auth/login`,
  REFRESH_JWT_TOKENS: `${environment.backendApiUrl}/auth/refresh-tokens`,
  LOGOUT: `${environment.backendApiUrl}/auth/log-out`,
} as const;

@Injectable()
export class AuthHttpClient {
  constructor(private http: HttpClient) {}

  register(dataForRegister: UserDataForRegister): Observable<UserWithTokens> {
    const reqBody: RegisterRequest = {
      email: dataForRegister.email,
      password: dataForRegister.password,
      name: dataForRegister.name,
    };

    return this.http
      .post<AuthResponse>(apiUrls.REGISTER, reqBody)
      .pipe(map(authResponseToUserWithTokens));
  }

  logIn(dataForLogIn: UserDataForLogIn): Observable<UserWithTokens> {
    const reqBody: LogInRequest = {
      email: dataForLogIn.email,
      password: dataForLogIn.password,
    };
    return this.http
      .post<AuthResponse>(apiUrls.LOGIN, reqBody)
      .pipe(map(authResponseToUserWithTokens));
  }

  refreshSession(): Observable<UserWithTokens> {
    return this.http
      .post<AuthResponse>(apiUrls.REFRESH_JWT_TOKENS, null)
      .pipe(map(authResponseToUserWithTokens));
  }

  logOut(): Observable<HttpResponse<string>> {
    return this.http.post<string>(apiUrls.LOGOUT, null, {
      observe: 'response',
    });
  }
}
