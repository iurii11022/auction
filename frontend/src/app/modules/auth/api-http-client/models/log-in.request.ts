export type LogInRequest = {
  email: string;
  password: string;
};
