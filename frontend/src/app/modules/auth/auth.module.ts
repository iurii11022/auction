import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { StoreModule } from '@ngrx/store';
import { AuthHttpClient } from './api-http-client/auth.http.client';
import { authReducer } from './state-management/auth.reducers';
import { AuthGuard } from '../../core-modules/guards/auth.guard';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './state-management/auth.effects';
import { authStoreName } from '#src/app/modules/auth/state-management/models/auth.store-name';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '#src/app/modules/auth/components/login-register/login.component';
import { RegisterComponent } from '#src/app/modules/auth/components/login-register/register.component';
import { ServerNotificationComponent } from '#src/app/modules/auth/components/notification/server-notification.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature(authStoreName, authReducer),
    EffectsModule.forFeature([AuthEffects]),
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    ServerNotificationComponent,
  ],
  exports: [],
  providers: [AuthHttpClient, AuthGuard],
})
export class AuthModule {}
