import { createAction, props } from '@ngrx/store';
import { User } from '../models/user';
import {
  LogInActionType,
  LogOutActionType,
  refreshSessionActionType,
  RegisterActionTypes,
} from '#src/app/modules/auth/state-management/models/auth.action-types';
import { UserDataForRegister } from '#src/app/modules/auth/models/user-data-for-register';
import { BackendError } from '#src/app/models/backend-errors';
import { UserDataForLogIn } from '#src/app/modules/auth/models/user-data-for-log-in';
import { AuthErrorMessage } from '#src/app/modules/auth/models/auth-error-message';

export const loginAction = createAction(
  LogInActionType.LOGIN,
  props<{ accountDataToLogIn: UserDataForLogIn }>()
);

export const loginSuccessAction = createAction(
  LogInActionType.LOGIN_SUCCESS,
  props<{ user: User }>()
);

export const loginFailureAction = createAction(
  LogInActionType.LOGIN_FAILURE,
  props<{ errors: AuthErrorMessage }>()
);

export const logoutAction = createAction(LogOutActionType.LOGOUT);

export const logoutSuccessAction = createAction(
  LogOutActionType.LOGOUT_SUCCESS
);

export const registerAction = createAction(
  RegisterActionTypes.REGISTER,
  props<{ accountDataToRegister: UserDataForRegister }>()
);

export const registerSuccessAction = createAction(
  RegisterActionTypes.REGISTER_SUCCESS,
  props<{ user: User }>()
);

export const registerFailureAction = createAction(
  RegisterActionTypes.REGISTER_FAILURE,
  props<{ errors: AuthErrorMessage }>()
);

export const refreshSessionAction = createAction(
  refreshSessionActionType.REFRESH_JWT_TOKENS
);

export const refreshSessionSuccessAction = createAction(
  refreshSessionActionType.REFRESH_SESSION_SUCCESS,
  props<{ user: User }>()
);

export const refreshSessionFailureAction = createAction(
  refreshSessionActionType.REFRESH_SESSION_FAILURE,
  props<{ errors: BackendError }>()
);
