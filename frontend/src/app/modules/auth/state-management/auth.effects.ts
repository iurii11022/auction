import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';

import {
  loginAction,
  loginFailureAction,
  loginSuccessAction,
  logoutAction,
  logoutSuccessAction,
  refreshSessionAction,
  refreshSessionFailureAction,
  refreshSessionSuccessAction,
  registerAction,
  registerFailureAction,
  registerSuccessAction,
} from '#src/app/modules/auth/state-management/auth.actions';
import { AuthHttpClient } from '#src/app/modules/auth/api-http-client/auth.http.client';
import { HttpErrorResponse } from '@angular/common/http';
import { JwtStorage } from '#src/app/core-modules/jwt/Jwt.storage';
import { UserWithTokens } from '#src/app/modules/auth/models/user-with-tokens';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private authHttpClient: AuthHttpClient
  ) {}

  register = createEffect(() =>
    this.actions$.pipe(
      ofType(registerAction),
      switchMap(({ accountDataToRegister }) => {
        return this.authHttpClient.register(accountDataToRegister).pipe(
          map(({ id, email, name, ...tokens }: UserWithTokens) => {
            JwtStorage.setTokens(tokens);
            return registerSuccessAction({ user: { id, name, email } });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              registerFailureAction({ errors: errorResponse.error.message })
            );
          })
        );
      })
    )
  );

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginAction),
      switchMap(({ accountDataToLogIn }) => {
        return this.authHttpClient.logIn(accountDataToLogIn).pipe(
          map(({ id, email, name, ...tokens }: UserWithTokens) => {
            JwtStorage.setTokens(tokens);
            return loginSuccessAction({ user: { id, name, email } });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              loginFailureAction({ errors: errorResponse.error.message })
            );
          })
        );
      })
    )
  );

  refreshSession$ = createEffect(() =>
    this.actions$.pipe(
      ofType(refreshSessionAction),
      switchMap(() => {
        return this.authHttpClient.refreshSession().pipe(
          map(({ id, email, name, ...tokens }: UserWithTokens) => {
            return refreshSessionSuccessAction({
              user: { id, name, email },
            });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              refreshSessionFailureAction({
                errors: errorResponse.error.errors,
              })
            );
          })
        );
      })
    )
  );

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(logoutAction),
      map(() => {
        JwtStorage.deleteTokens();
        return logoutSuccessAction();
      })
    )
  );

  redirectAfterSubmit$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loginSuccessAction, registerSuccessAction),
        tap(() => {
          this.router.navigateByUrl('/');
        })
      ),
    { dispatch: false }
  );

  redirectAfterLogout$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(logoutSuccessAction),
        tap(() => {
          this.router.navigateByUrl('/');
        })
      ),
    { dispatch: false }
  );
}
