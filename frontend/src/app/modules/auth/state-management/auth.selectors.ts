import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState } from '#src/app/modules/auth/state-management/models/auth.state';
import { authStoreName } from '#src/app/modules/auth/state-management/models/auth.store-name';

export const selectAuthState = createFeatureSelector<AuthState>(authStoreName);

export const isLoggedIn = createSelector(
  selectAuthState,
  (auth: AuthState) => !!auth.user
);

export const isLoggedOut = createSelector(
  selectAuthState,
  (auth: AuthState) => !auth.user
);

export const nickname = createSelector(
  selectAuthState,
  (auth: AuthState) => auth.user?.name
);

export const authErrors = createSelector(
  selectAuthState,
  (auth: AuthState) => auth.errors
);

export const currentAccount = createSelector(
  selectAuthState,
  (auth: AuthState) => auth.user
);
