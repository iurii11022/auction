import { User } from '#src/app/modules/auth/models/user';
import { AuthErrorMessage } from '#src/app/modules/auth/models/auth-error-message';

export type AuthState = {
  user: User | null;
  errors: AuthErrorMessage | null;
};
