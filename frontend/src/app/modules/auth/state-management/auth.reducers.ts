import { createReducer, on } from '@ngrx/store';
import { AuthState } from '#src/app/modules/auth/state-management/models/auth.state';
import {
  loginFailureAction,
  loginSuccessAction,
  logoutAction,
  refreshSessionSuccessAction,
  registerFailureAction,
  registerSuccessAction,
} from '#src/app/modules/auth/state-management/auth.actions';

export const initialAuthState: AuthState = {
  user: null,
  errors: null,
};

export const authReducer = createReducer(
  initialAuthState,

  on(
    loginSuccessAction,
    (state, action): AuthState => ({
      ...state,
      user: action.user,
    })
  ),

  on(
    registerSuccessAction,
    (state, action): AuthState => ({
      ...state,
      user: action.user,
      errors: null,
    })
  ),

  on(
    registerFailureAction,
    (state, { errors }): AuthState => ({
      ...state,
      user: null,
      errors: errors,
    })
  ),

  on(
    loginFailureAction,
    (state, { errors }): AuthState => ({
      ...state,
      user: null,
      errors: errors,
    })
  ),

  on(logoutAction, state => ({
    ...initialAuthState,
  })),

  on(
    refreshSessionSuccessAction,
    (state, action): AuthState => ({
      ...state,
      user: action.user,
    })
  )
);
