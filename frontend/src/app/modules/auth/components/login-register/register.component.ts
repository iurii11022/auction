import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { registerAction } from '../../state-management/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './register.component.html',
  styleUrls: ['./login-register.component.scss'],
})
export class RegisterComponent implements OnInit {
  form!: UntypedFormGroup;

  constructor(private fb: UntypedFormBuilder, private store: Store) {}

  ngOnInit() {
    this.initializeForm();
  }

  private initializeForm(): void {
    this.form = this.fb.group({
      email: ['user1@angular.com', [Validators.required, Validators.email]],
      password: ['test1234', [Validators.required]],
      name: ['John', [Validators.required]],
    });
  }

  onSubmitRegisterForm(): void {
    const props = {
      accountDataToRegister: this.form.value,
    };

    this.store.dispatch(registerAction(props));
  }
}
