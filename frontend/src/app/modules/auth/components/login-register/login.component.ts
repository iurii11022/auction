import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';

import { Store } from '@ngrx/store';

import { loginAction } from '../../state-management/auth.actions';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login-register.component.scss'],
})
export class LoginComponent implements OnInit {
  form: UntypedFormGroup;

  constructor(private fb: UntypedFormBuilder, private store: Store) {
    this.form = fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.initializeForm();
  }

  private initializeForm(): void {
    this.form = this.fb.group({
      email: ['user1@angular.com', [Validators.required, Validators.email]],
      password: ['test1234', [Validators.required]],
    });
  }

  onSubmitLoginForm(): void {
    const accountDataToLogInProps = {
      accountDataToLogIn: this.form.value,
    };

    this.store.dispatch(loginAction(accountDataToLogInProps));
  }
}
