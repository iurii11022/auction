import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { authErrors } from '#src/app/modules/auth/state-management/auth.selectors';
import { Store } from '@ngrx/store';
import { AuthErrorMessage } from '#src/app/modules/auth/models/auth-error-message';

@Component({
  selector: 'app-server-notification',
  templateUrl: './server-notification.component.html',
  styleUrls: ['./server-notification.component.scss'],
})
export class ServerNotificationComponent implements OnInit, OnDestroy {
  private authErrorsSubscription?: Subscription;
  authErrors$: Observable<AuthErrorMessage>;

  constructor(private snackBar: MatSnackBar, private store: Store) {
    this.authErrors$ = this.store.select(authErrors);
  }

  ngOnInit() {
    this.authErrorsSubscription = this.authErrors$
      .pipe(
        tap(errors => {
          if (errors && errors.length > 0) {
            console.log(errors, 'ServerNotificationComponent');
            this.openSnackBar(errors[0], 'close');
          }
        })
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.authErrorsSubscription.unsubscribe();
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      duration: 5 * 1000,
    });
  }
}
