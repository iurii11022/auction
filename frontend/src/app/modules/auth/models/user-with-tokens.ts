import { User } from '#src/app/modules/auth/models/user';

export type UserWithTokens = User & {
  accessToken: string;
  refreshToken: string;
};
