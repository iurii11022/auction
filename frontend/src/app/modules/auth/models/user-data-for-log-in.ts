export type UserDataForLogIn = {
  email: string;
  password: string;
};
