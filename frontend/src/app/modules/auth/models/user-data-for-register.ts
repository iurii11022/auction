export type UserDataForRegister = {
  email: string;
  password: string;
  name: string;
};
