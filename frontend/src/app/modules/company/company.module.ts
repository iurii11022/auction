import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule, Routes } from '@angular/router';
import { CompanyComponent } from '#src/app/modules/company/components/company.component';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { CompanyEffects } from '#src/app/modules/company/state-management/company.effects';
import { companyStoreName } from '#src/app/modules/company/state-management/models/company.store-name';
import { companyReducer } from '#src/app/modules/company/state-management/company.reducers';
import { CompanyHttpClient } from '#src/app/modules/company/api-http-client/company-http-client.service';
import { CommonModule } from '@angular/common';
import { SelectorModule } from '#src/app/core-modules/selector/selector.module';
import { CompanyService } from '#src/app/modules/company/services/company.service';
import { CompanyResolver } from '#src/app/modules/company/services/company-resolver.service';
import { MatTabsModule } from '@angular/material/tabs';
const routes: Routes = [
  {
    path: '',
    component: CompanyComponent,
    resolve: {
      company: CompanyResolver,
    },
  },
];

@NgModule({
  imports: [
    MatTabsModule,
    SelectorModule,
    CommonModule,
    MatCardModule,
    MatButtonModule,
    RouterModule.forChild(routes),
    EffectsModule.forFeature([CompanyEffects]),
    StoreModule.forFeature(companyStoreName, companyReducer),
  ],
  declarations: [CompanyComponent],
  exports: [],
  providers: [CompanyHttpClient, CompanyService, CompanyResolver],
})
export class CompanyModule {}
