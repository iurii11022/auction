import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Company } from '#src/app/modules/company/models/company';
import { CompanyService } from '#src/app/modules/company/services/company.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss'],
})
export class CompanyComponent implements OnInit {
  company$: Observable<Company | null>;

  constructor(private companyService: CompanyService) {}

  ngOnInit(): void {
    this.company$ = this.companyService.company$;
  }

  updateCompany(company: Company): void {
    this.companyService.updateCompanyById(company);
  }
}
