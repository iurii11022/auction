import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  isCompanyLoaded,
  selectCompany,
} from '#src/app/modules/company/state-management/company.selectors';
import { CompanyId } from '#src/app/modules/company/models/company-id';
import {
  getCompanyByIdAction,
  updateCompanyByIdAction,
} from '#src/app/modules/company/state-management/company.actions';
import { Company } from '#src/app/modules/company/models/company';

@Injectable()
export class CompanyService {
  loaded$: Observable<boolean>;
  company$: Observable<Company>;

  constructor(private store: Store) {
    this.loaded$ = this.store.pipe(select(isCompanyLoaded));
    this.company$ = this.store.select(selectCompany);
  }

  public getCompanyById(companyId: CompanyId): void {
    this.store.dispatch(getCompanyByIdAction({ companyId }));
  }

  public updateCompanyById(company: Company): void {
    this.store.dispatch(updateCompanyByIdAction({ company }));
  }
}
