import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { finalize, Observable } from 'rxjs';
import { filter, first, tap } from 'rxjs/operators';
import { CompanyService } from '#src/app/modules/company/services/company.service';

@Injectable()
export class CompanyResolver implements Resolve<boolean> {
  private loading = false;

  constructor(private companyService: CompanyService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    const companyId = route.params['companyId'];
    return this.companyService.loaded$.pipe(
      tap(loaded => {
        if (!this.loading && !loaded) {
          this.loading = true;
          this.companyService.getCompanyById(companyId);
        }
      }),
      filter(loaded => !!loaded),
      first(),
      finalize(() => (this.loading = false))
    );
  }
}
