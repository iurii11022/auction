import { CompanyType } from '#src/app/modules/company/models/company-type';
import { CompanyPhoto } from '#src/app/modules/company/models/company-photo/company-photo';
import { CompanyId } from '#src/app/modules/company/models/company-id';

export type Company = {
  id: CompanyId;
  email: string;
  name: string;
  description?: string;
  created_at: Date;
  updated_at: Date;
  logo: string;
  photos: CompanyPhoto[];
  type: CompanyType;
};
