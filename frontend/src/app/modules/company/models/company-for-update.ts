import { CompanyId } from '#src/modules/company/models/company-id';

export type CompanyForUpdate = {
  id: CompanyId;
  email?: string;
  password?: string;
};
