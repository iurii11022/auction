import { Opaque } from '#src/utils/types/opaque';

export type CompanyPhotoId = Opaque<string, 'CompanyPhotoId'>;
