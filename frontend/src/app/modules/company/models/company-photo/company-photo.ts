import { CompanyId } from '#src/app/modules/company/models/company-id';
import { CompanyPhotoId } from '#src/app/modules/company/models/company-photo/company-photo-id';

export type CompanyPhoto = {
  id: CompanyPhotoId;
  name: string;
  company_id: CompanyId;
};
