import { Opaque } from '#src/utils/types/opaque';

export type CompanyId = Opaque<string, 'CompanyId'>;
