import { createAction, props } from '@ngrx/store';
import { CompanyId } from '#src/app/modules/company/models/company-id';
import { Company } from '#src/app/modules/company/models/company';
import { BackendError } from '#src/app/models/backend-errors';
import {
  GetCompanyByIdActionTypes,
  UpdateCompanyByIdActionTypes,
} from '#src/app/modules/company/state-management/models/company.action-types';

export const getCompanyByIdAction = createAction(
  GetCompanyByIdActionTypes.GET_COMPANY_BY_ID,
  props<{ companyId: CompanyId }>()
);

export const getCompanyByIdSuccessAction = createAction(
  GetCompanyByIdActionTypes.GET_COMPANY_BY_ID_SUCCESS,
  props<{ company: Company }>()
);

export const getCompanyByIdFailureAction = createAction(
  GetCompanyByIdActionTypes.GET_COMPANY_BY_ID_FAILURE,
  props<{ errors: BackendError }>()
);

export const updateCompanyByIdAction = createAction(
  UpdateCompanyByIdActionTypes.UPDATE_COMPANY_BY_ID,
  props<{ company: Company }>()
);

export const updateCompanyByIdSuccessAction = createAction(
  UpdateCompanyByIdActionTypes.UPDATE_COMPANY_BY_ID_SUCCESS,
  props<{ company: Company }>()
);

export const updateCompanyByIdFailureAction = createAction(
  UpdateCompanyByIdActionTypes.UPDATE_COMPANY_BY_ID_FAILURE,
  props<{ errors: BackendError }>()
);
