import { companyStoreName } from '#src/app/modules/company/state-management/models/company.store-name';

export const GetCompanyByIdActionTypes = {
  GET_COMPANY_BY_ID: `[${companyStoreName}] Get Company By Id`,
  GET_COMPANY_BY_ID_SUCCESS: `[${companyStoreName}] Get Company By Id Success`,
  GET_COMPANY_BY_ID_FAILURE: `[${companyStoreName}] Get Company By Id Failure`,
} as const;

export const UpdateCompanyByIdActionTypes = {
  UPDATE_COMPANY_BY_ID: `[${companyStoreName}] Update Company By Id`,
  UPDATE_COMPANY_BY_ID_SUCCESS: `[${companyStoreName}] Update Company By Id Success`,
  UPDATE_COMPANY_BY_ID_FAILURE: `[${companyStoreName}] Update Company By Id Failure`,
} as const;
