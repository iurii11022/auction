import { Company } from '#src/app/modules/company/models/company';

export type CompanyState = {
  company: Company | null;
};
