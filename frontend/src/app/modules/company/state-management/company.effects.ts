import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, switchMap } from 'rxjs/operators';

import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { CompanyHttpClient } from '#src/app/modules/company/api-http-client/company-http-client.service';
import {
  getCompanyByIdAction,
  getCompanyByIdFailureAction,
  getCompanyByIdSuccessAction,
  updateCompanyByIdAction,
  updateCompanyByIdFailureAction,
  updateCompanyByIdSuccessAction,
} from '#src/app/modules/company/state-management/company.actions';

@Injectable()
export class CompanyEffects {
  constructor(
    private actions$: Actions,
    private companyHttpClient: CompanyHttpClient
  ) {}

  getCompanyById = createEffect(() =>
    this.actions$.pipe(
      ofType(getCompanyByIdAction),
      switchMap(({ companyId }) => {
        return this.companyHttpClient.getCompanyById(companyId).pipe(
          map(company => {
            console.log(company, 'company');
            return getCompanyByIdSuccessAction({ company });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              getCompanyByIdFailureAction({
                errors: errorResponse.error.errors,
              })
            );
          })
        );
      })
    )
  );

  updateCompanyById = createEffect(() =>
    this.actions$.pipe(
      ofType(updateCompanyByIdAction),
      switchMap(({ company }) => {
        return this.companyHttpClient.updateCompanyById(company).pipe(
          map(company => {
            return updateCompanyByIdSuccessAction({ company });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              updateCompanyByIdFailureAction({
                errors: errorResponse.error.errors,
              })
            );
          })
        );
      })
    )
  );
}
