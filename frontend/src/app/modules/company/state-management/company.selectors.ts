import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CompanyState } from '#src/app/modules/company/state-management/models/company.state';
import { companyStoreName } from '#src/app/modules/company/state-management/models/company.store-name';

export const selectCompanyState =
  createFeatureSelector<CompanyState>(companyStoreName);

export const selectCompany = createSelector(
  selectCompanyState,
  (state: CompanyState) => state.company
);
export const isCompanyLoaded = createSelector(
  selectCompanyState,
  (state: CompanyState) => !!state.company
);
