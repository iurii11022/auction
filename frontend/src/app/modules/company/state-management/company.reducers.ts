import { createReducer, on } from '@ngrx/store';
import { CompanyState } from '#src/app/modules/company/state-management/models/company.state';
import {
  getCompanyByIdFailureAction,
  getCompanyByIdSuccessAction,
  updateCompanyByIdFailureAction,
  updateCompanyByIdSuccessAction,
} from '#src/app/modules/company/state-management/company.actions';

export const initialCompanyState: CompanyState = {
  company: null,
};

export const companyReducer = createReducer(
  initialCompanyState,

  on(
    getCompanyByIdSuccessAction,
    (state, action): CompanyState => ({
      ...state,
      company: action.company,
    })
  ),

  on(getCompanyByIdFailureAction, state => ({
    ...initialCompanyState,
  })),

  on(
    updateCompanyByIdSuccessAction,
    (state, action): CompanyState => ({
      ...state,
      company: action.company,
    })
  ),

  on(updateCompanyByIdFailureAction, state => ({
    ...initialCompanyState,
  }))
);
