import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '#src/environments/environment';
import { CompanyId } from '#src/app/modules/company/models/company-id';
import { Company } from '#src/app/modules/company/models/company';

const apiUrls = {
  COMPANY: `${environment.backendApiUrl}/company`,
} as const;

@Injectable()
export class CompanyHttpClient {
  constructor(private http: HttpClient) {}

  getCompanyById(companyId: CompanyId): Observable<Company> {
    return this.http.get<Company>(apiUrls.COMPANY + '/' + companyId);
  }

  updateCompanyById(company: Company): Observable<Company> {
    return this.http.put<Company>(apiUrls.COMPANY, company);
  }
}
