export type AuthResponse = {
  id: string;
  email: string;
  name: string;
  access_token: string;
  refresh_token: string;
  token_type: string;
  expires_in: number;
};
