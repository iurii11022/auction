import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '#src/environments/environment';
import { AuctionToCreate } from '#src/app/modules/auctions/models/auction-to-create';

import { Auction } from '#src/app/modules/auctions/models/auction';
import { AuctionToUpdate } from '#src/app/modules/auctions/models/auction-to-update';
import { PictureLoad } from '#src/app/modules/auctions/models/picture-load';
import { Picture } from '#src/app/modules/auctions/models/picture';
import { AuctionId } from '#src/app/modules/auctions/models/auction-id';
import { map } from 'rxjs/operators';
import { FileResponse } from '#src/app/models/file-response';

const apiUrls = {
  AUCTION: `${environment.backendApiUrl}/auction`,
  PICTURE: `${environment.backendApiUrl}/picture`,
} as const;

@Injectable()
export class AuctionHttpClient {
  constructor(private http: HttpClient) {}

  createAuction(auction: AuctionToCreate): Observable<Auction> {
    return this.http.post<Auction>(apiUrls.AUCTION, auction);
  }

  updateAuction(id: AuctionId, auction: AuctionToUpdate): Observable<Auction> {
    console.log(auction, 'auction');
    return this.http.put<Auction>(apiUrls.AUCTION + '/' + id, auction);
  }

  getAllAuctions(searchValue: string): Observable<Auction[]> {
    const params = new HttpParams({ fromObject: { searchValue } });

    return this.http.get<Auction[]>(apiUrls.AUCTION, { params });
  }

  updateAuctionPicture(picture: PictureLoad): Observable<Picture> {
    const fd = new FormData();
    fd.append('image', picture.file);

    return this.http.post<FileResponse>(apiUrls.PICTURE, fd).pipe(
      map(p => {
        return {
          id: picture.id,
          url: p.url,
        };
      })
    );
  }
}
