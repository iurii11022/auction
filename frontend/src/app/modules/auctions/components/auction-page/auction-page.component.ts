import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { map } from 'rxjs/operators';
import { Auction } from '#src/app/modules/auctions/models/auction';
import { AuctionsStoreService } from '#src/app/modules/auctions/services/auctions-store.service';
import { dialogConfig } from '#src/app/modules/auctions/components/utils/dialog-config';
import { AuctionDialogComponent } from '#src/app/modules/auctions/components/auction-dialog/auction-dialog.component';

@Component({
  selector: 'auction-page',
  templateUrl: './auction-page.component.html',
  styleUrls: ['./auction-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuctionPageComponent implements OnInit {
  public allAuction$!: Observable<Auction[]>;
  public auctionsAmount$!: Observable<number>;

  constructor(
    private dialog: MatDialog,
    private auctionStoreService: AuctionsStoreService
  ) {}

  ngOnInit() {
    this.initValues();
  }

  initValues() {
    this.allAuction$ = this.auctionStoreService.entities$.pipe(
      map(entities => Object.values(entities))
    );
    this.auctionsAmount$ = this.auctionStoreService.auctionsAmount$;
  }

  createNewAuction() {
    const config = dialogConfig();

    this.dialog
      .open(AuctionDialogComponent, config)
      .afterClosed()
      .subscribe(() => this.initValues());
  }

  onSearch(searchValue: string) {
    this.auctionStoreService.getAuctions(searchValue);
  }
}
