import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuctionsStoreService } from '#src/app/modules/auctions/services/auctions-store.service';
import { AuctionToCreate } from '#src/app/modules/auctions/models/auction-to-create';
import { AuctionType } from '#src/app/modules/auctions/models/auction-type';
import { Observable, take } from 'rxjs';
import { Auction } from '#src/app/modules/auctions/models/auction';

@Component({
  selector: 'app-auction-dialog',
  templateUrl: './auction-dialog.component.html',
  styleUrls: ['./auction-dialog.component.scss'],
})
export class AuctionDialogComponent implements OnInit {
  loading$: Observable<boolean>;
  public isLinear = false;
  public currentAuction: Observable<Auction | null>;
  public selectedAuctionType: string;
  public auctionType = [
    { value: AuctionType.DUTCH, viewValue: AuctionType.DUTCH },
    { value: AuctionType.ENGLISH, viewValue: AuctionType.ENGLISH },
  ];

  public auctionForm!: FormGroup;
  public auctionObjectForm!: FormGroup;
  public auctionAdditionalInfoForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AuctionDialogComponent>,
    private auctionStoreService: AuctionsStoreService
  ) {
    this.currentAuction = this.auctionStoreService.getCurrentAuction();
  }

  ngOnInit() {
    this.initForm();
  }

  public selectedPictureFile: File = null;
  public selectedPictureFileId: string;

  onPictureSelected(id: string) {
    console.log(id, 'this.selectedPictureFileId ididid');
    const pictureId: string = '#' + id;
    const inputNode: any = document.querySelector(pictureId);

    if (typeof FileReader !== 'undefined') {
      this.selectedPictureFileId = id;
      this.selectedPictureFile = inputNode.files[0];
    }
  }

  onPictureSubmit() {
    console.log(this.selectedPictureFileId, 'this.selectedPictureFileId');
    this.auctionStoreService.updatePicture({
      id: this.selectedPictureFileId,
      file: this.selectedPictureFile,
    });
  }

  initForm() {
    const auctionFormControls = {
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(16),
        ],
      ],
      type: ['', [Validators.required]],
      description: [''],
      planned_start: [
        '',
        [Validators.required, this.dateValidator, this.isDateGreaterThanToday],
      ],
      planned_end: [
        '',
        [Validators.required, this.dateValidator, this.isDateGreaterThanToday],
      ],
    };

    const auctionObjectFormControls = {
      object_name: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(18),
        ],
      ],
      desired_price: ['', [Validators.required]],
      min_price: ['', [Validators.required]],
      max_price: ['', [Validators.required]],
      object_info: [''],
    };

    const auctionAdditionalInfoFormControls = {
      additional_info: [''],
      main_photo: ['', [Validators.required, Validators.pattern('http')]],
      object_photo_1: [''],
      object_photo_2: [''],
    };

    this.auctionForm = this.fb.group({
      ...auctionFormControls,
    });

    this.auctionObjectForm = this.fb.group({
      ...auctionObjectFormControls,
    });

    this.auctionAdditionalInfoForm = this.fb.group({
      ...auctionAdditionalInfoFormControls,
    });

    this.currentAuction.pipe().subscribe((auction: Auction) => {
      if (auction) {
        this.auctionForm.patchValue({
          name: auction.name,
          type: auction.type,
          description: auction.description,
          planned_start: auction.planned_start,
          planned_end: auction.planned_end,
        });

        this.auctionObjectForm.patchValue({
          object_name: auction.object_name,
          desired_price: auction.desired_price,
          min_price: auction.min_price,
          max_price: auction.max_price,
          object_info: auction.object_info,
        });

        this.auctionAdditionalInfoForm.patchValue({
          additional_info: auction.additional_info,
          main_photo: auction.main_photo,
          object_photo_1: auction.object_photo_1,
          object_photo_2: auction.object_photo_2,
        });
      }
    });
  }

  onClose() {
    this.dialogRef.close();
    this.auctionStoreService.clearCurrentAuction();
  }

  update(close?: string) {
    let auction: AuctionToCreate = {
      ...this.auctionForm.value,
      planned_start: new Date(this.auctionForm.value.planned_start),
      planned_end: new Date(this.auctionForm.value.planned_end),
    };

    this.currentAuction.pipe(take(1)).subscribe(existingAuction => {
      if (existingAuction) {
        auction = {
          ...this.auctionForm.value,
          planned_start: new Date(this.auctionForm.value.planned_start),
          planned_end: new Date(this.auctionForm.value.planned_end),
          ...this.auctionObjectForm.value,
          desired_price: +this.auctionObjectForm.value.desired_price,
          min_price: +this.auctionObjectForm.value.min_price,
          max_price: +this.auctionObjectForm.value.max_price,
          ...this.auctionAdditionalInfoForm.value,
        };
        console.log(auction, 'existingAuction');

        this.auctionStoreService.updateAuction(existingAuction.id, auction);
      } else {
        this.auctionStoreService.createAuction(auction);
      }
    });

    if (close) {
      this.onClose();
    }
  }

  private dateValidator(
    control: AbstractControl
  ): { [key: string]: boolean } | null {
    const value = control.value;
    if (!value) {
      return null;
    }

    const date = new Date(value);
    if (isNaN(date.getTime())) {
      return { invalidDate: true };
    }

    return null;
  }

  private isDateGreaterThanToday(
    control: AbstractControl
  ): { [key: string]: boolean } | null {
    const value = control.value;
    const today = new Date();
    today.setHours(0, 0, 0, 0); // set the time to start of the day

    const selectedDate = new Date(value);
    if (selectedDate <= today) {
      return { dateNotGreaterThanToday: true };
    }

    return null;
  }
}
