import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Auction } from '#src/app/modules/auctions/models/auction';
import { Router } from '@angular/router';

@Component({
  selector: 'auction-card-list',
  templateUrl: './auction-card-list.component.html',
  styleUrls: ['./auction-card-list.component.scss'],
})
export class AuctionCardListComponent {
  @Input()
  auctions!: Auction[];

  @Output()
  auctionChanged = new EventEmitter();

  constructor(private router: Router) {}

  navigateToAuction(auctionId: string) {
    this.router.navigate(['/chat', auctionId]);
  }
}
