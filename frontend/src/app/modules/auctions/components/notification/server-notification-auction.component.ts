import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AuthErrorMessage } from '#src/app/modules/auth/models/auth-error-message';
import { getCurrentAuctionErrors } from '#src/app/modules/auctions/state-management/auctions.selectors';

@Component({
  selector: 'app-server-auction-notification',
  templateUrl: './server-notification-auction.component.html',
  styleUrls: ['./server-notification-auction.component.scss'],
})
export class ServerNotificationAuctionComponent implements OnInit, OnDestroy {
  private authErrorsSubscription?: Subscription;
  authErrors$: Observable<AuthErrorMessage>;

  constructor(private snackBar: MatSnackBar, private store: Store) {
    this.authErrors$ = this.store.select(getCurrentAuctionErrors);
  }

  ngOnInit() {
    this.authErrorsSubscription = this.authErrors$
      .pipe(
        tap(errors => {
          if (errors && errors.length > 0) {
            this.openSnackBar(errors[0], 'close');
          }
        })
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.authErrorsSubscription.unsubscribe();
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      duration: 5 * 1000,
    });
  }
}
