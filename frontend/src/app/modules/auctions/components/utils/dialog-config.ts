import { MatDialogConfig } from '@angular/material/dialog';

export function dialogConfig() {
  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;
  dialogConfig.minWidth = '96%';
  dialogConfig.minHeight = '85%';
  dialogConfig.maxHeight = '90%';
  dialogConfig.position = {
    left: '50px',
  };

  return dialogConfig;
}
