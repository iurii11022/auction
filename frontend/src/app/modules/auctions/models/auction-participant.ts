import { Auction } from '#src/app/modules/auctions/models/auction';
import { AuctionParticipantId } from '#src/app/modules/auctions/models/auction-participant-id';
import { AuctionParticipantRole } from '#src/app/modules/auctions/models/auction-participant-role';
import { User } from '#src/app/modules/auth/models/user';

export type AuctionParticipant = {
  id: AuctionParticipantId;
  auction: Auction;
  company: User;
  company_alias: string;
  role: AuctionParticipantRole;
  last_seen: Date;
};
