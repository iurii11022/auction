export enum AuctionParticipantRole {
  BUYER = 'buyer',
  SUPPLIER = 'supplier',
}
