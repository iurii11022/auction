export enum AuctionStatus {
  CREATED = 'created',
  STARTED = 'started',
  FINISHED = 'finished',
  DECLINED = 'declined',
}
