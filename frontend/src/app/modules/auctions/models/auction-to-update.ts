import { AuctionType } from '#src/app/modules/auctions/models/auction-type';
import { AuctionStatus } from '#src/app/modules/auctions/models/auction-status';

export type AuctionToUpdate = {
  name?: string;
  type?: AuctionType;
  description?: string;
  planned_start?: Date;
  planned_end?: Date;
  status?: AuctionStatus;
  object_name?: string;
  object_info?: string;
  desired_price?: number;
  min_price?: number;
  max_price?: number;
  additional_info?: string;
  main_photo?: string;
  object_photo_1?: string;
  object_photo_2?: string;
};
