import { Opaque } from '#src/utils/types/opaque';

export type AuctionParticipantId = Opaque<string, 'AuctionParticipantId'>;
