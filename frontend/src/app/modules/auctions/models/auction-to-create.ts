import { AuctionType } from '#src/app/modules/auctions/models/auction-type';

export type AuctionToCreate = {
  name: string;
  type: AuctionType;
  description?: string;
  planned_start: Date;
  planned_end: Date;
};
