export type PictureLoad = {
  id: string;
  file: File;
};
