import { Opaque } from '#src/utils/types/opaque';

export type AuctionId = Opaque<string, 'AuctionId'>;
