import { createReducer, on } from '@ngrx/store';

import {
  getAuctionsSuccessAction,
  createAuctionSuccessAction,
  updateAuctionSuccessAction,
  clearCurrentAuction,
  updateAuctionPictureSuccessAction,
  updateAuctionPictureFailureAction,
  updateAuctionFailureAction,
  createAuctionFailureAction,
} from '#src/app/modules/auctions/state-management/auction.actions';
import { createEntityAdapter } from '@ngrx/entity';
import { Auction } from '#src/app/modules/auctions/models/auction';
import { AuctionsState } from '#src/app/modules/auctions/state-management/models/auctions-state';

export const auctionAdapter = createEntityAdapter<Auction>();

export const initialAuctionsState: AuctionsState =
  auctionAdapter.getInitialState({
    auctionsAmount: 0,
    isAuctionsLoaded: false,
    currentAuction: null,
    currentAuctionErrors: null,
  });

export const auctionReducer = createReducer(
  initialAuctionsState,

  on(
    getAuctionsSuccessAction,
    (
      state,
      { auctions }: { auctions: Auction[]; type: string }
    ): AuctionsState =>
      auctionAdapter.setAll(auctions, {
        ...state,
        isAuctionsLoaded: true,
      })
  ),

  on(createAuctionSuccessAction, (state, { createdAuction }) => {
    return {
      ...auctionAdapter.addOne(createdAuction, state),
      currentAuction: createdAuction,
      currentAuctionErrors: null,
    };
  }),

  on(createAuctionFailureAction, (state, { errors }) => {
    return {
      ...state,
      currentAuctionErrors: errors,
    };
  }),

  on(updateAuctionSuccessAction, (state, { auction }) => {
    return {
      ...auctionAdapter.upsertOne(auction, state),
      currentAuction: auction,
      currentAuctionErrors: null,
    };
  }),

  on(updateAuctionFailureAction, (state, { errors }) => {
    return {
      ...state,
      currentAuctionErrors: errors,
    };
  }),

  on(updateAuctionPictureSuccessAction, (state, { picture }) => {
    let updatedAuction = { ...state.currentAuction };

    switch (picture.id) {
      case 'main_photo':
        updatedAuction.main_photo = picture.url;
        break;
      case 'object_photo_1':
        updatedAuction.object_photo_1 = picture.url;
        break;
      case 'object_photo_2':
        updatedAuction.object_photo_2 = picture.url;
        break;
    }

    return {
      ...state,
      currentAuction: updatedAuction,
      currentAuctionErrors: null,
    };
  }),

  on(updateAuctionPictureFailureAction, (state, { errors }) => {
    return {
      ...state,
      currentAuctionErrors: errors,
    };
  }),

  on(clearCurrentAuction, state => {
    return {
      ...state,
      currentAuction: null,
    };
  })
);
