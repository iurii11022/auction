import { createAction, props } from '@ngrx/store';

import { BackendError } from '#src/app/models/backend-errors';
import { Auction } from '#src/app/modules/auctions/models/auction';
import { AuctionToCreate } from '#src/app/modules/auctions/models/auction-to-create';
import {
  createAuctionActionType,
  currentAuctionActionType,
  getAuctionsActionType,
  updateAuctionActionType,
  updateAuctionPictureActionType,
} from '#src/app/modules/auctions/state-management/models/auctions.action-types';
import { AuctionToUpdate } from '#src/app/modules/auctions/models/auction-to-update';
import { PictureLoad } from '#src/app/modules/auctions/models/picture-load';
import { Picture } from '#src/app/modules/auctions/models/picture';
import { AuctionId } from '#src/app/modules/auctions/models/auction-id';
import { AuctionDialogErrorMessage } from '#src/app/modules/auctions/models/auction-dialog-error-message';

export const getAuctionsAction = createAction(
  getAuctionsActionType.GET_AUCTIONS,
  props<{ searchValue: string }>()
);

export const getAuctionsSuccessAction = createAction(
  getAuctionsActionType.GET_AUCTIONS_SUCCESS,
  props<{ auctions: Auction[] }>()
);

export const getAuctionsFailureAction = createAction(
  getAuctionsActionType.GET_AUCTIONS_FAILURE,
  props<{ errors: BackendError }>()
);

export const createAuctionAction = createAction(
  createAuctionActionType.CREATE_AUCTION,
  props<{ auctionToCreate: AuctionToCreate }>()
);

export const createAuctionSuccessAction = createAction(
  createAuctionActionType.CREATE_AUCTION_SUCCESS,
  props<{ createdAuction: Auction }>()
);

export const createAuctionFailureAction = createAction(
  createAuctionActionType.CREATE_AUCTION_FAILURE,
  props<{ errors: AuctionDialogErrorMessage }>()
);

export const clearCurrentAuction = createAction(
  currentAuctionActionType.CLEAR_CURRENT_AUCTION
);

export const updateAuctionPictureAction = createAction(
  updateAuctionPictureActionType.UPDATE_AUCTION_PICTURE,
  props<{ picture: PictureLoad }>()
);

export const updateAuctionPictureSuccessAction = createAction(
  updateAuctionPictureActionType.UPDATE_AUCTION_SUCCESS_PICTURE,
  props<{ picture: Picture }>()
);

export const updateAuctionPictureFailureAction = createAction(
  updateAuctionPictureActionType.UPDATE_AUCTION_FAILURE_PICTURE,
  props<{ errors: AuctionDialogErrorMessage }>()
);

export const updateAuctionAction = createAction(
  updateAuctionActionType.UPDATE_AUCTION,
  props<{ id: AuctionId; auctionToUpdate: AuctionToUpdate }>()
);

export const updateAuctionSuccessAction = createAction(
  updateAuctionActionType.UPDATE_AUCTION_SUCCESS,
  props<{ auction: Auction }>()
);

export const updateAuctionFailureAction = createAction(
  updateAuctionActionType.UPDATE_AUCTION_FAILURE,
  props<{ errors: AuctionDialogErrorMessage }>()
);
