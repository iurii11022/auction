import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, concatMap, map } from 'rxjs/operators';

import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { AuctionsService } from '#src/app/modules/auctions/services/auctions.service';
import {
  getAuctionsAction,
  getAuctionsFailureAction,
  getAuctionsSuccessAction,
  createAuctionAction,
  createAuctionSuccessAction,
  createAuctionFailureAction,
  updateAuctionAction,
  updateAuctionSuccessAction,
  updateAuctionFailureAction,
  updateAuctionPictureAction,
  updateAuctionPictureSuccessAction,
  updateAuctionPictureFailureAction,
} from '#src/app/modules/auctions/state-management/auction.actions';
import { Auction } from '#src/app/modules/auctions/models/auction';
import { Picture } from '#src/app/modules/auctions/models/picture';

@Injectable()
export class AuctionsEffects {
  constructor(
    private actions$: Actions,
    private auctionService: AuctionsService
  ) {}

  getAuctions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getAuctionsAction),
      concatMap(({ searchValue }) => {
        return this.auctionService.getAllAuctions(searchValue).pipe(
          map((auctions: Auction[]) => {
            return getAuctionsSuccessAction({ auctions });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              getAuctionsFailureAction({ errors: errorResponse.error.message })
            );
          })
        );
      })
    )
  );

  createAuction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createAuctionAction),
      concatMap(({ auctionToCreate }) => {
        return this.auctionService.createAuction(auctionToCreate).pipe(
          map((auction: Auction) => {
            return createAuctionSuccessAction({ createdAuction: auction });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              createAuctionFailureAction({
                errors: errorResponse.error.message,
              })
            );
          })
        );
      })
    )
  );

  updateAuction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateAuctionAction),
      concatMap(({ id, auctionToUpdate }) => {
        return this.auctionService.updateAuction(id, auctionToUpdate).pipe(
          map((auction: Auction) => {
            return updateAuctionSuccessAction({ auction });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              updateAuctionFailureAction({
                errors: errorResponse.error.message,
              })
            );
          })
        );
      })
    )
  );

  updateAuctionPicture$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateAuctionPictureAction),
      concatMap(({ picture }) => {
        return this.auctionService.updateAuctionPicture(picture).pipe(
          map((picture: Picture) => {
            return updateAuctionPictureSuccessAction({ picture });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              updateAuctionPictureFailureAction({
                errors: errorResponse.error.message,
              })
            );
          })
        );
      })
    )
  );
}
