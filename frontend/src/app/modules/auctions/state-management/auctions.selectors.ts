import { createFeatureSelector, createSelector } from '@ngrx/store';
import { auctionsStoreName } from '#src/app/modules/auctions/state-management/models/auctions-store-name';
import { AuctionsState } from '#src/app/modules/auctions/state-management/models/auctions-state';

export const selectAuctionsState =
  createFeatureSelector<AuctionsState>(auctionsStoreName);

export const isAuctionsLoaded = createSelector(
  selectAuctionsState,
  (state: AuctionsState) => state.isAuctionsLoaded
);

export const getAuctionEntities = createSelector(
  selectAuctionsState,
  (state: AuctionsState) => state.entities
);

export const getAuctionsAmount = createSelector(
  selectAuctionsState,
  (state: AuctionsState) => state.auctionsAmount
);

export const getCurrentAuction = createSelector(
  selectAuctionsState,
  (state: AuctionsState) => state.currentAuction
);

export const getCurrentAuctionErrors = createSelector(
  selectAuctionsState,
  (state: AuctionsState) => state.currentAuctionErrors
);

export const getAuctionById = (id: string) =>
  createSelector(selectAuctionsState, (state: AuctionsState) => {
    return state.entities[id];
  });
