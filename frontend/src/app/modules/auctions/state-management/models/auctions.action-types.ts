import { auctionsStoreName } from '#src/app/modules/auctions/state-management/models/auctions-store-name';

export const getAuctionsActionType = {
  GET_AUCTIONS: `[${auctionsStoreName}] get auctions`,
  GET_AUCTIONS_SUCCESS: `[${auctionsStoreName}] get auctions Success`,
  GET_AUCTIONS_FAILURE: `[${auctionsStoreName}] get auctions Failure`,
} as const;

export const createAuctionActionType = {
  CREATE_AUCTION: `[${auctionsStoreName}] create auction`,
  CREATE_AUCTION_SUCCESS: `[${auctionsStoreName}] create auction Success`,
  CREATE_AUCTION_FAILURE: `[${auctionsStoreName}] create auction Failure`,
} as const;

export const updateAuctionActionType = {
  UPDATE_AUCTION: `[${auctionsStoreName}] update auction`,
  UPDATE_AUCTION_SUCCESS: `[${auctionsStoreName}] update auction Success`,
  UPDATE_AUCTION_FAILURE: `[${auctionsStoreName}] update auction Failure`,
} as const;

export const currentAuctionActionType = {
  CLEAR_CURRENT_AUCTION: `[${auctionsStoreName}] clear current auction`,
} as const;

export const updateAuctionPictureActionType = {
  UPDATE_AUCTION_PICTURE: `[${auctionsStoreName}] update auction picture`,
  UPDATE_AUCTION_SUCCESS_PICTURE: `[${auctionsStoreName}] update auction Success picture`,
  UPDATE_AUCTION_FAILURE_PICTURE: `[${auctionsStoreName}] update auction Failure picture`,
} as const;
