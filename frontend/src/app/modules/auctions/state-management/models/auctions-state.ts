import { Auction } from '#src/app/modules/auctions/models/auction';
import { EntityState } from '@ngrx/entity';
import { AuctionDialogErrorMessage } from '#src/app/modules/auctions/models/auction-dialog-error-message';

export type AuctionsState = EntityState<Auction> & {
  auctionsAmount: number;
  isAuctionsLoaded: boolean;
  currentAuction: Auction | null;
  currentAuctionErrors: AuctionDialogErrorMessage | null;
};
