import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { finalize, Observable } from 'rxjs';
import { filter, first, tap } from 'rxjs/operators';
import { AuctionsStoreService } from '#src/app/modules/auctions/services/auctions-store.service';

@Injectable()
export class AuctionsResolver implements Resolve<boolean> {
  private loading = false;

  constructor(private auctionStoreService: AuctionsStoreService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.auctionStoreService.loaded$.pipe(
      tap(loaded => {
        if (!this.loading && !loaded) {
          this.loading = true;
          this.auctionStoreService.loadDefaultAuctionsPage();
        }
      }),
      filter(loaded => !!loaded),
      first(),
      finalize(() => (this.loading = false))
    );
  }
}
