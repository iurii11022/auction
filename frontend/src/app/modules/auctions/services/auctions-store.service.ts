import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Auction } from '#src/app/modules/auctions/models/auction';
import {
  getAuctionEntities,
  getAuctionsAmount,
  getCurrentAuction,
  isAuctionsLoaded,
} from '#src/app/modules/auctions/state-management/auctions.selectors';
import {
  getAuctionsAction,
  createAuctionAction,
  updateAuctionAction,
  clearCurrentAuction,
  updateAuctionPictureAction,
} from '#src/app/modules/auctions/state-management/auction.actions';
import { AuctionToCreate } from '#src/app/modules/auctions/models/auction-to-create';
import { AuctionToUpdate } from '#src/app/modules/auctions/models/auction-to-update';
import { PictureLoad } from '#src/app/modules/auctions/models/picture-load';
import { AuctionId } from '#src/app/modules/auctions/models/auction-id';

@Injectable()
export class AuctionsStoreService {
  public entities$: Observable<{ [id: string]: Auction }>;
  public auctionsAmount$: Observable<number>;
  public loaded$: Observable<boolean>;

  constructor(private store: Store) {
    this.loaded$ = this.store.pipe(select(isAuctionsLoaded));
    this.entities$ = this.store.pipe(select(getAuctionEntities));
    this.auctionsAmount$ = this.store.pipe(select(getAuctionsAmount));
  }

  public createAuction(auctionToCreate: AuctionToCreate) {
    this.store.dispatch(createAuctionAction({ auctionToCreate }));
  }

  public updateAuction(id: AuctionId, auctionToUpdate: AuctionToUpdate) {
    this.store.dispatch(updateAuctionAction({ id, auctionToUpdate }));
  }

  public updatePicture(picture: PictureLoad): void {
    this.store.dispatch(updateAuctionPictureAction({ picture }));
  }

  public clearCurrentAuction() {
    this.store.dispatch(clearCurrentAuction());
  }

  public loadDefaultAuctionsPage(): void {
    this.getAuctions('');
  }

  public getAuctions(searchValue: string): void {
    this.store.dispatch(getAuctionsAction({ searchValue }));
  }

  public getCurrentAuction(): Observable<Auction> {
    return this.store.pipe(select(getCurrentAuction));
  }
}
