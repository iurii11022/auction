import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuctionHttpClient } from '#src/app/modules/auctions/api-http-client/auction.http.client';
import { Auction } from '#src/app/modules/auctions/models/auction';
import { AuctionToCreate } from '#src/app/modules/auctions/models/auction-to-create';
import { AuctionToUpdate } from '#src/app/modules/auctions/models/auction-to-update';
import { PictureLoad } from '#src/app/modules/auctions/models/picture-load';
import { Picture } from '#src/app/modules/auctions/models/picture';
import { AuctionId } from '#src/app/modules/auctions/models/auction-id';

@Injectable()
export class AuctionsService {
  constructor(private http: AuctionHttpClient) {}

  public getAllAuctions(searchValue: string): Observable<Auction[]> {
    return this.http.getAllAuctions(searchValue);
  }

  public createAuction(auctionToCreate: AuctionToCreate): Observable<Auction> {
    return this.http.createAuction(auctionToCreate);
  }

  public updateAuction(
    id: AuctionId,
    auctionToUpdate: AuctionToUpdate
  ): Observable<Auction> {
    return this.http.updateAuction(id, auctionToUpdate);
  }
  public updateAuctionPicture(picture: PictureLoad): Observable<Picture> {
    return this.http.updateAuctionPicture(picture);
  }
}
