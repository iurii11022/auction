import { NgModule } from '@angular/core';
import { CommonModule, NgFor } from '@angular/common';
import { AuctionCardListComponent } from './components/auction-card-list/auction-card-list.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule, Routes } from '@angular/router';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SearchModule } from '#src/app/core-modules/search/search.module';
import { SelectorModule } from '#src/app/core-modules/selector/selector.module';
import { AuctionPageComponent } from '#src/app/modules/auctions/components/auction-page/auction-page.component';
import { AuctionsResolver } from '#src/app/modules/auctions/services/auctions-resolver.service';
import { auctionReducer } from '#src/app/modules/auctions/state-management/auction.reducers';
import { auctionsStoreName } from '#src/app/modules/auctions/state-management/models/auctions-store-name';
import { AuctionDialogComponent } from '#src/app/modules/auctions/components/auction-dialog/auction-dialog.component';
import { AuctionsService } from '#src/app/modules/auctions/services/auctions.service';
import { AuctionsStoreService } from '#src/app/modules/auctions/services/auctions-store.service';
import { AuctionHttpClient } from '#src/app/modules/auctions/api-http-client/auction.http.client';
import { AuctionsEffects } from '#src/app/modules/auctions/state-management/auctions-effects';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ServerNotificationAuctionComponent } from '#src/app/modules/auctions/components/notification/server-notification-auction.component';

export const auctionRoutes: Routes = [
  {
    path: '',
    component: AuctionPageComponent,
    resolve: {
      auction: AuctionsResolver,
    },
  },
];

@NgModule({
  imports: [
    FormsModule,
    NgFor,
    MatStepperModule,
    MatFormFieldModule,
    SelectorModule,
    SearchModule,
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatTabsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    ReactiveFormsModule,
    RouterModule.forChild(auctionRoutes),
    EffectsModule.forFeature([AuctionsEffects]),
    StoreModule.forFeature(auctionsStoreName, auctionReducer),
  ],
  declarations: [
    AuctionPageComponent,
    AuctionCardListComponent,
    AuctionDialogComponent,
    ServerNotificationAuctionComponent,
  ],
  exports: [AuctionPageComponent, AuctionCardListComponent],
  providers: [
    AuctionHttpClient,
    AuctionsService,
    AuctionsStoreService,
    AuctionsResolver,
  ],
})
export class AuctionsModule {}
