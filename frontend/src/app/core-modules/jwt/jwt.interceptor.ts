import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtStorage } from '#src/app/core-modules/jwt/Jwt.storage';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = JwtStorage.getToken('accessToken');

    const request = req.clone({
      headers: req.headers.set('Authorization', token ?? ''),
    });
    return next.handle(request);
  }
}
