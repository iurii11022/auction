import { Injectable } from '@angular/core';

type Tokens = {
  accessToken: string;
  refreshToken: string;
};

@Injectable()
export class JwtStorage {
  public static setTokens(tokens: Tokens): void {
    this.setToken('accessToken', tokens.accessToken);
    this.setToken('refreshToken', tokens.refreshToken);
  }

  public static getToken(key: string): string | null {
    try {
      return localStorage.getItem(key);
    } catch (e) {
      console.error(e);
      return null;
    }
  }

  public static deleteTokens(): void {
    try {
      localStorage.removeItem('accessToken');
      localStorage.removeItem('refreshToken');
    } catch (e) {
      console.error(e);
    }
  }

  private static setToken(key: string, data: string): void {
    try {
      localStorage.setItem(key, data);
    } catch (e) {
      console.error(e);
    }
  }
}
