import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-search-component',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent {
  value = '';

  @Output() searchValueChange = new EventEmitter<string>();

  onInputChange(event: Event): void {
    this.value = (event.target as HTMLInputElement).value;
    this.searchValueChange.emit(this.value);
  }

  onClearButtonClicked() {
    this.value = '';
    this.searchValueChange.emit(this.value);
  }
}
