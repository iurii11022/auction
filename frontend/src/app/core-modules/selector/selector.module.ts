import { NgModule } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgFor } from '@angular/common';
import { SelectorComponent } from '#src/app/core-modules/selector/PL/selector.component';

@NgModule({
  imports: [
    MatFormFieldModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgFor,
  ],
  declarations: [SelectorComponent],
  exports: [SelectorComponent],
})
export class SelectorModule {}
