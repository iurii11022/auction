import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-select-multiple',
  templateUrl: 'selector.component.html',
})
export class SelectorComponent {
  @Input() toppingName: string = '';
  @Input() toppingList: string[] = [];
  @Output() toppingsChange = new EventEmitter<string[]>();
  @Output() closed = new EventEmitter<void>();

  toppings = new FormControl([]);

  // Emit the change event whenever the value of the form control changes
  ngOnInit() {
    // @ts-ignore
    this.toppings.valueChanges.subscribe((value: string[]) => {
      this.toppingsChange.emit(value);
    });
  }

  // Emit the closed event when the select panel is closed
  onClosed() {
    this.closed.emit();
  }
}
